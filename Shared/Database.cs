﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Shared.Models.Definitions;
using Shared.Models.Utils;
using Shared.Models.WorldStart;

namespace Shared
{
    public class Database
    {
        public readonly SQLiteAsyncConnection Connection;
        public static Database Instance;

        public Database(string dbFilePath)
        {
            Connection = new SQLiteAsyncConnection(dbFilePath);
            Console.WriteLine("Initializing SQLite DB at: {0}", dbFilePath);
        }

        public async Task Init()
        {
            await Connection.CreateTableAsync<Player>();
            await Connection.CreateTableAsync<Room>();
            await Connection.CreateTableAsync<Door>();
            await Connection.CreateTableAsync<NPC>();
            await Connection.CreateTableAsync<WaresCollection>();
            await Connection.CreateTableAsync<WaresEntry>();
            await Connection.CreateTableAsync<StatBlock>();
            await Connection.CreateTableAsync<LootTable>();
            await Connection.CreateTableAsync<LootTableEntry>();
            await Connection.CreateTableAsync<Item>();
            await Connection.CreateTableAsync<Spell>();
            await Connection.CreateTableAsync<Trap>();
            await Connection.CreateTableAsync<QuestStep>();
            await Connection.CreateTableAsync<QuestProgress>();
            await Connection.CreateTableAsync<Faction>();
            await Connection.CreateTableAsync<StartingNPC>();
            await Connection.CreateTableAsync<StartingItem>();
            await Connection.CreateTableAsync<StartingInventory>();

            Instance = this;
        }
    }
}
