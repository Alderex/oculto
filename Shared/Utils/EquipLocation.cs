﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Utils
{
    public class EquipLocation
    {
        public static int WEAPON = 0;
        public static int OFFHAND = 1;
        public static int HEAD = 2;
        public static int CHEST = 3;
        public static int HANDS = 4;
        public static int LEGS = 5;
        public static int FEET = 6;
        public static int NECK = 7;
        public static int RING = 8;
        public static int BACK = 9;
    }
}
