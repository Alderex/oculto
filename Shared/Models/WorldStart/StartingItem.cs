﻿using Shared.Models.Definitions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.WorldStart
{
    public class StartingItem
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string SourceID { get; set; }
        public string RoomID { get; set; }

        public string Details { get { return itemName; } }
        private string itemName;

        public StartingItem() { }

        public StartingItem(string id, string sourceID, string roomID)
        {
            ID = id;
            SourceID = sourceID;
            RoomID = roomID;

            _ = RefreshDetails();
        }

        public async Task RefreshDetails()
        {
            Item item = await Item.Get(SourceID);
            itemName = item.Name;
        }

        public static async Task<StartingItem> Get(string ID)
        {
            return await Database.Instance.Connection.Table<StartingItem>().Where(s => s.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<StartingItem>> GetAll()
        {
            return await Database.Instance.Connection.Table<StartingItem>().OrderBy(s => s.RoomID).ToListAsync();
        }

        public static async Task<int> Assert(StartingItem startingItem)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(startingItem);
        }

        public static async Task<int> Delete(StartingItem startingItem)
        {
            return await Database.Instance.Connection.DeleteAsync(startingItem);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<StartingItem>();
        }

        public static async Task<int> DeleteAllForRoom(Room room)
        {
            return await Database.Instance.Connection.Table<StartingItem>().DeleteAsync(s => s.RoomID == room.ID);
        }

        public static async Task<List<StartingItem>> GetStartingItemsInRoom(Definitions.Room room)
        {
            return await Database.Instance.Connection.Table<StartingItem>().Where(n => n.RoomID == room.ID).ToListAsync();
        }

        public async Task Save()
        {
            await Assert(this);
        }
    }
}
