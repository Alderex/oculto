﻿using Shared.Models.Definitions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.WorldStart
{
    public class StartingInventory
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string SourceID { get; set; }
        public string PlayerID { get; set; }

        public string Details { get { return itemName; } }
        private string itemName;

        public StartingInventory() { }

        public StartingInventory(string id, string sourceID, string playerID)
        {
            ID = id;
            SourceID = sourceID;
            PlayerID = playerID;
        }

        public async Task RefreshDetails()
        {
            Item item = await Item.Get(SourceID);
            itemName = item.Name;
        }

        public static async Task<StartingInventory> Get(string ID)
        {
            return await Database.Instance.Connection.Table<StartingInventory>().Where(s => s.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<StartingInventory>> GetAll()
        {
            return await Database.Instance.Connection.Table<StartingInventory>().ToListAsync();
        }

        public static async Task<int> Assert(StartingInventory startingInventory)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(startingInventory);
        }

        public static async Task<int> Delete(StartingInventory startingInventory)
        {
            return await Database.Instance.Connection.DeleteAsync(startingInventory);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<StartingInventory>();
        }

        public static async Task<int> DeleteAllForPlayer(string playerID)
        {
            return await Database.Instance.Connection.Table<StartingInventory>().DeleteAsync(x => x.PlayerID == playerID);
        }

        public static async Task<List<StartingInventory>> GetStartingInventoryForPlayer(string playerID)
        {
            return await Database.Instance.Connection.Table<StartingInventory>().Where(s => s.PlayerID == playerID).ToListAsync();
        }

        public async Task Save()
        {
            await Assert(this);
        }
    }
}
