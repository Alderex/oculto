﻿using Shared.Models.Definitions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.WorldStart
{
    public class StartingNPC
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string SourceID { get; set; }
        public string RoomID { get; set; }

        public string Details { get { return npcName; } }
        private string npcName;

        public StartingNPC() { }

        public StartingNPC(string id, string sourceID, string roomID)
        {
            ID = id;
            SourceID = sourceID;
            RoomID = roomID;

            _ = RefreshDetails();
        }

        public async Task RefreshDetails()
        {
            NPC npc = await NPC.Get(SourceID);
            npcName = npc.Name;
        }

        public static async Task<StartingNPC> Get(string ID)
        {
            return await Database.Instance.Connection.Table<StartingNPC>().Where(s => s.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<StartingNPC>> GetAll()
        {
            return await Database.Instance.Connection.Table<StartingNPC>().OrderBy(s => s.RoomID).ToListAsync();
        }

        public static async Task<int> Assert(StartingNPC startingNPC)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(startingNPC);
        }

        public static async Task<int> Delete(StartingNPC startingNPC)
        {
            return await Database.Instance.Connection.DeleteAsync(startingNPC);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<StartingNPC>();
        }

        public static async Task<int> DeleteAllForRoom(Room room)
        {
            return await Database.Instance.Connection.Table<StartingNPC>().DeleteAsync(n => n.RoomID == room.ID);
        }

        public static async Task<List<StartingNPC>> GetStartingNPCsForRoom(Definitions.Room room)
        {
            return await Database.Instance.Connection.Table<StartingNPC>().Where(n => n.RoomID == room.ID).ToListAsync();
        }

        public async Task Save()
        {
            await Assert(this);
        }
    }
}
