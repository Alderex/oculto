﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Definitions
{
    public class Trap
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string Name { get; set; }
        public string SavingThrow { get; set; }
        public int SavingThrowRequirement { get; set; }
        public bool AdvanceOnFailure { get; set; }
        public string OnSuccessMessage { get; set; }
        public string OnFailureMessage { get; set; }
        public int MinDamage { get; set; }
        public int MaxDamage { get; set; }
        public string FailureDestinationID { get; set; }

        public Trap()
        { }


        public static async Task<Trap> Get(string ID)
        {
            return await Database.Instance.Connection.Table<Trap>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<Trap>> GetAll()
        {
            return await Database.Instance.Connection.Table<Trap>().OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<int> Assert(Trap trap)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(trap);
        }

        public static async Task<int> Delete(Trap trap)
        {
            return await Database.Instance.Connection.DeleteAsync(trap);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<Trap>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Trap))
                return false;

            return ((Trap)obj).ID == ID;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
