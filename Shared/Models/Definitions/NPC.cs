﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Definitions
{
    public class NPC
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string StatBlockID { get; set; }
        public string WaresCollectionID { get; set; }
        public string LootTableID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IntroPhrase { get; set; }
        public int WaresRepRequirement { get; set; }
        public string WaresRejectionPhrase { get; set; }
        public int RespawnTimer { get; set; }

        public NPC() { }

        public static async Task<NPC> Get(string ID)
        {
            return await Database.Instance.Connection.Table<NPC>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<NPC>> GetAll()
        {
            return await Database.Instance.Connection.Table<NPC>().OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<int> Assert(NPC npc)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(npc);
        }

        public static async Task<int> Delete(NPC npc)
        {
            return await Database.Instance.Connection.DeleteAsync(npc);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<NPC>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public async Task<Utils.StatBlock> GetStatblock()
        {
            return await Database.Instance.Connection.Table<Utils.StatBlock>().Where(x => x.ID == StatBlockID).FirstOrDefaultAsync();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is NPC))
                return false;

            return ((NPC)obj).ID == ID;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
