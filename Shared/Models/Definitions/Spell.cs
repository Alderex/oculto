﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Definitions
{
    public class Spell
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string OnUseOutput { get; set; }
        public int ManaCost { get; set; }
        public int SpellType { get; set; }
        public int Amount { get; set; }

        public Spell() { }

        public static async Task<Spell> Get(string ID)
        {
            return await Database.Instance.Connection.Table<Spell>().Where(s => s.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<Spell>> GetAll()
        {
            return await Database.Instance.Connection.Table<Spell>().OrderBy(s => s.Name).ToListAsync();
        }

        public static async Task<Spell> GetByName(string spellName)
        {
            return await Database.Instance.Connection.Table<Spell>().Where(s => s.Name == spellName).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(Spell spell)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(spell);
        }

        public static async Task<int> Delete(Spell spell)
        {
            return await Database.Instance.Connection.DeleteAsync(spell);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<Spell>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Spell))
                return false;

            return ((Spell)obj).ID == ID;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
