﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Definitions
{
    public class Room
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Area { get; set; }

        public Room()
        { }

        public static async Task<Room> Get(string ID)
        {
            return await Database.Instance.Connection.Table<Room>().Where(r => r.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<Room>> GetAll()
        {
            return await Database.Instance.Connection.Table<Room>().OrderBy(r => r.Name).ToListAsync();
        }

        public static async Task<int> Assert(Room room)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(room);
        }

        public static async Task<int> Delete(Room room)
        {
            return await Database.Instance.Connection.DeleteAsync(room);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<Room>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Room))
                return false;

            return ((Room)obj).ID == ID;
        }

        public override string ToString()
        {
            return Name + " [" + Area + "]";
        }
    }
}
