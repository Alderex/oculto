﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Definitions
{
    public class Door
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string RoomID { get; set; }
        public string TrapID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string OnUseOutput { get; set; }
        public string RoomDestination { get; set; }
        public int PerceptionRequired { get; set; }
        public string RequiredKey { get; set; }

        public Door()
        { }


        public static async Task<Door> Get(string ID)
        {
            return await Database.Instance.Connection.Table<Door>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<Door>> GetAll()
        {
            return await Database.Instance.Connection.Table<Door>().OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<int> Assert(Door door)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(door);
        }

        public static async Task<int> Delete(Door door)
        {
            return await Database.Instance.Connection.DeleteAsync(door);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<Door>();
        }

        public static async Task<int> DeleteAllInRoom(Room room)
        {
            return await Database.Instance.Connection.Table<Door>().DeleteAsync(d => d.RoomID == room.ID);
        }

        public static async Task<List<Door>> GetDoorsInRoom(Room room)
        {
            return await Database.Instance.Connection.Table<Door>().Where(d => d.RoomID == room.ID).ToListAsync();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Door))
                return false;

            return ((Door)obj).ID == ID;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
