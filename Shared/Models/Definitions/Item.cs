﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Definitions
{
    public class Item
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string ImbuedSpellID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Unidentified { get; set; }
        public int DamageMin { get; set; }
        public int DamageMax { get; set; }
        public int AttackRollBonus { get; set; }
        public int Armor { get; set; }
        public int DexterityReq { get; set; }
        public int StrengthReq { get; set; }
        public int IntelligenceReq { get; set; }
        public int DexterityBonus { get; set; }
        public int StrengthBonus { get; set; }
        public int IntelligenceBonus { get; set; }
        public int WisdomBonus { get; set; }
        public int CharismaBonus { get; set; }
        public int ConstitutionBonus { get; set; }
        public string OnUseEffect { get; set; }
        public string OnUseParameter { get; set; }
        public string OnUseOutput { get; set; }
        public bool ConsumeOnUse { get; set; }
        public int Price { get; set; }
        public int EquipLocation { get; set; }
        public int PerceptionRequired { get; set; }
        public int Weight { get; set; }

        public Item() { }

        public static async Task<Item> Get(string ID)
        {
            return await Database.Instance.Connection.Table<Item>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<Item>> GetAll()
        {
            return await Database.Instance.Connection.Table<Item>().OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<int> Assert(Item item)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(item);
        }

        public static async Task<int> Delete(Item item)
        {
            return await Database.Instance.Connection.DeleteAsync(item);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<Item>();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Item))
                return false;

            return ((Item)obj).ID == ID;
        }

        public override string ToString()
        {
            return Name;
        }

        public async Task<string> WaresOutput(double charismaDiscount)
        {
            string output = "";
            output += "--- " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Name) + " ---";
            output += "\n" + Description;

            if (DamageMax != 0)
                output += "\nDamage: " + DamageMin + "-" + DamageMax;
            if (Armor != 0)
                output += "\nArmor: " + Armor;
            if (StrengthReq != 0)
                output += "\nRequired Strength: " + StrengthReq;
            if (DexterityReq != 0)
                output += "\nRequired Dexterity: " + DexterityReq;
            if (IntelligenceReq != 0)
                output += "\nRequired Intelligence: " + IntelligenceReq;
            if (StrengthBonus != 0)
                output += "\nStrength Bonus: " + StrengthBonus;
            if (DexterityBonus != 0)
                output += "\nDexterity Bonus: " + DexterityBonus;
            if (IntelligenceBonus != 0)
                output += "\nIntelligence Bonus: " + IntelligenceBonus;
            if (WisdomBonus != 0)
                output += "\nWisdom Bonus: " + WisdomBonus;
            if (CharismaBonus != 0)
                output += "\nCharisma Bonus: " + CharismaBonus;
            if (ConstitutionBonus != 0)
                output += "\nConstitution Bonus: " + ConstitutionBonus;

            // Check if this item has an imbued spell
            if (ImbuedSpellID != null)
            {
                Spell spell = await Spell.Get(ImbuedSpellID);
                if (spell != null)
                    output += "\nImbued Spell: " + spell.Name + " (" + spell.Description + ")";
            }

            if (Price != 0)
                output += "\nCost: " + (int)(Price * charismaDiscount) + " gold";

            return output;
        }
    }
}
