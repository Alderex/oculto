﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Definitions
{
    public class Player
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string Name { get; set; }

        public int Strength { get; set; } // Boosts carrying capacity and proficiency with STR based weapons
        public int Constitution { get; set; } // Boosts hit points
        public int Charisma { get; set; } // Better shop prices and lower rep requirements
        public int Intelligence { get; set; } // Increases mana and spell proficiency
        public int Wisdom { get; set; } // Better search results
        public int Dexterity { get; set; }  // Increases AC (dodge)

        public int Gold { get; set; }

        public Player() {}

        public static async Task<Player> Get(string ID)
        {
            return await Database.Instance.Connection.Table<Player>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<Player> GetByName(string name)
        {
            return await Database.Instance.Connection.Table<Player>().Where(x => x.Name == name).FirstOrDefaultAsync();
        }

        public static async Task<List<Player>> GetAll()
        {
            return await Database.Instance.Connection.Table<Player>().OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<int> Assert(Player player)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(player);
        }

        public static async Task<int> Delete(Player player)
        {
            return await Database.Instance.Connection.DeleteAsync(player);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<Player>();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Player))
                return false;

            return ((Player)obj).ID == ID;
        }
    }
}
