﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Utils
{
    public class StatBlock
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string FactionID { get; set; }
        public string Name { get; set; }
        public int HitPoints { get; set; }
        public int Mana { get; set; }
        public int AC { get; set; }
        public int MinAttack { get; set; }
        public int MaxAttack { get; set; }
        public int AttackRollBonus { get; set; }
        public int FactionDeathPenalty { get; set; }
        public int DeathXP { get; set; }

        public StatBlock() { }

        public static async Task<StatBlock> Get(string ID)
        {
            return await Database.Instance.Connection.Table<StatBlock>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<StatBlock>> GetAll()
        {
            return await Database.Instance.Connection.Table<StatBlock>().OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<int> Assert(StatBlock statBlock)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(statBlock);
        }

        public static async Task<int> Delete(StatBlock statBlock)
        {
            return await Database.Instance.Connection.DeleteAsync(statBlock);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<StatBlock>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is StatBlock))
                return false;

            return ((StatBlock)obj).ID == ID;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
