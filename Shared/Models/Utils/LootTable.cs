﻿using Shared.Models.Definitions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Utils
{
    public class LootTable
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string Name { get; set; }

        public LootTable() { }

        public static async Task<LootTable> Get(string ID)
        {
            return await Database.Instance.Connection.Table<LootTable>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<LootTable>> GetAll()
        {
            return await Database.Instance.Connection.Table<LootTable>().OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<int> Assert(LootTable lootTable)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(lootTable);
        }

        public static async Task<int> Delete(LootTable lootTable)
        {
            return await Database.Instance.Connection.DeleteAsync(lootTable);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<LootTable>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public async Task<List<Item>> GetLootDrops()
        {
            Random rand = new Random();

            // The lower the drop val, the better
            int dropVal = rand.Next(0, 100);

            List<LootTableEntry> lootTableEntries = await Database.Instance.Connection.Table<LootTableEntry>().Where(x => x.LootTableID == ID && x.DropChance >= dropVal).ToListAsync();

            List<Item> items = new List<Item>();
            foreach(LootTableEntry entry in lootTableEntries)
            {
                Item i = await Item.Get(entry.ItemID);
                items.Add(i);
            }

            return items;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
