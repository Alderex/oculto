﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Utils
{
    public class QuestProgress
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string QuestStepID { get; set; }
        public string PlayerID { get; set; }
        public string NpcID { get; set; }

        public QuestProgress() { }

        public static async Task<QuestProgress> Get(string ID)
        {
            return await Database.Instance.Connection.Table<QuestProgress>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<QuestProgress>> GetAllForPlayer(string playerID)
        {
            return await Database.Instance.Connection.Table<QuestProgress>().Where(x => x.PlayerID == playerID).ToListAsync();
        }

        public static async Task<QuestProgress> GetPlayerProgressOnQuest(string playerID, string npcID)
        {
            return await Database.Instance.Connection.Table<QuestProgress>().Where(x => x.PlayerID == playerID && x.NpcID == npcID).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(QuestProgress questProgress)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(questProgress);
        }

        public static async Task<int> Delete(QuestProgress questProgress)
        {
            return await Database.Instance.Connection.DeleteAsync(questProgress);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<QuestProgress>();
        }

        public async Task Save()
        {
            await Assert(this);
        }
    }
}
