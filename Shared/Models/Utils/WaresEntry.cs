﻿using Shared.Models.Definitions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Utils
{
    public class WaresEntry
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string WaresCollectionID { get; set; }
        public string ItemID { get; set; }

        [Ignore]
        public string Details { get { return name; } }
        private string name;

        public WaresEntry() { }

        public WaresEntry(string id, string waresCollectionID, string itemID) {
            ID = id;
            WaresCollectionID = waresCollectionID;
            ItemID = itemID;
            _ = RefreshDetails();
        }

        public async Task RefreshDetails()
        {
            Item item = await Item.Get(ItemID);
            name = item.Name;
        }

        public static async Task<WaresEntry> Get(string ID)
        {
            return await Database.Instance.Connection.Table<WaresEntry>().Where(i => i.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<WaresEntry>> GetAllForCollection(string waresCollectionID)
        {
            return await Database.Instance.Connection.Table<WaresEntry>().Where(i => i.WaresCollectionID == waresCollectionID).ToListAsync();
        }

        public static async Task<int> Assert(WaresEntry wares)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(wares);
        }

        public static async Task<int> Delete(WaresEntry wares)
        {
            return await Database.Instance.Connection.DeleteAsync(wares);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<WaresEntry>();
        }

        public static async Task<int> DeleteAllForCollection(string waresCollectionID)
        {
            return await Database.Instance.Connection.Table<WaresEntry>().DeleteAsync(x => x.WaresCollectionID == waresCollectionID);
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public static async Task AddWaresToCollection(string collectionID, string itemID)
        {
            WaresEntry wares = new WaresEntry();
            wares.ID = Guid.NewGuid().ToString();
            wares.WaresCollectionID = collectionID;
            wares.ItemID = itemID;
            await Database.Instance.Connection.InsertOrReplaceAsync(wares);
        }

        public static async Task RemoveWaresFromCollection(string collectionID, string itemID)
        {
            WaresEntry wares = await Database.Instance.Connection.Table<WaresEntry>().Where(w => w.WaresCollectionID == collectionID && w.ItemID == itemID).FirstOrDefaultAsync();
            if (wares != null)
                await Delete(wares);
        }
    }
}
