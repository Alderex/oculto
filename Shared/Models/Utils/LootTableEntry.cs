﻿using Shared.Models.Definitions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Utils
{
    public class LootTableEntry
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string LootTableID { get; set; }
        public string ItemID { get; set; }
        public int DropChance { get; set; }

        [Ignore]
        public string Details { get { return String.Format("{0} - {1}%", name, DropChance.ToString()); } }
        private string name;

        public LootTableEntry() { }

        public LootTableEntry(string id, string lootTableID, string itemID, int dropChance)
        {
            ID = id;
            LootTableID = lootTableID;
            ItemID = itemID;
            DropChance = dropChance;
            _ = RefreshDetails();
        }

        public async Task RefreshDetails()
        {
            Item item = await Item.Get(ItemID);
            name = item.Name;
        }

        public static async Task<LootTableEntry> Get(string ID)
        {
            return await Database.Instance.Connection.Table<LootTableEntry>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<LootTableEntry>> GetAllForTable(string lootTableID)
        {
            return await Database.Instance.Connection.Table<LootTableEntry>().Where(x => x.LootTableID == lootTableID).ToListAsync();
        }

        public static async Task<int> Assert(LootTableEntry dropTableEntry)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(dropTableEntry);
        }

        public static async Task<int> Delete(LootTableEntry dropTableEntry)
        {
            return await Database.Instance.Connection.DeleteAsync(dropTableEntry);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<LootTableEntry>();
        }

        public static async Task<int> DeleteAllForTable(string lootTableID)
        {
            return await Database.Instance.Connection.Table<LootTableEntry>().DeleteAsync(x => x.LootTableID == lootTableID);
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public static async Task AddLootToTable(string dropTableID, string itemID)
        {
            LootTableEntry dropTableEntry = new LootTableEntry();
            dropTableEntry.ID = Guid.NewGuid().ToString();
            dropTableEntry.LootTableID = dropTableID;
            dropTableEntry.ItemID = itemID;
            await Database.Instance.Connection.InsertOrReplaceAsync(dropTableEntry);
        }

        public static async Task RemoveLootFromTable(string dropTableID, string itemID)
        {
            LootTableEntry loot = await Database.Instance.Connection.Table<LootTableEntry>().Where(x => x.LootTableID == dropTableID && x.ItemID == itemID).FirstOrDefaultAsync();
            if (loot != null)
                await Delete(loot);
        }
    }
}
