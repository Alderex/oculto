﻿using Shared.Models.Definitions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Utils
{
    public class QuestStep
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string NpcID { get; set; }
        public int StepNumber { get; set; }
        public string Description { get; set; }
        public int RepRequirement { get; set; }
        public string RepReqRejectionPhrase { get; set; }
        public string StartPhrase { get; set; }
        public string RewardPhrase { get; set; }
        public string RejectionPhrase { get; set; }
        public string WantedItem { get; set; }
        public int WantedItemQuantity { get; set; }
        public int RewardGold { get; set; }
        public string RewardItem { get; set; }
        public int RepReward { get; set; }
        public int RewardXP { get; set; }

        public QuestStep() { }

        public static async Task<QuestStep> Get(string ID)
        {
            return await Database.Instance.Connection.Table<QuestStep>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<QuestStep>> GetAll()
        {
            return await Database.Instance.Connection.Table<QuestStep>().OrderBy(x => x.Description).ToListAsync();
        }

        public static async Task<List<QuestStep>> GetAllForNPC(string npcID)
        {
            return await Database.Instance.Connection.Table<QuestStep>().Where(x => x.NpcID == npcID).OrderBy(x => x.StepNumber).ToListAsync();
        }

        public static async Task<QuestStep> GetCurrentQuestStepForPlayer(string npcID, string playerID)
        {
            QuestProgress questProgress = await Database.Instance.Connection.Table<QuestProgress>().Where(x => x.PlayerID == playerID && x.NpcID == npcID).FirstOrDefaultAsync();
            if(questProgress != null)
            {
                return await Database.Instance.Connection.Table<QuestStep>().Where(x => x.ID == questProgress.QuestStepID).FirstOrDefaultAsync();
            }
            else
            {
                // Check if this NPC actually has a quest, and if so, start progress for this player
                QuestStep firstStep = await Database.Instance.Connection.Table<QuestStep>().Where(x => x.NpcID == npcID).OrderBy(x => x.StepNumber).FirstOrDefaultAsync();
                if(firstStep != null)
                {
                    questProgress = new QuestProgress
                    {
                        ID = Guid.NewGuid().ToString(),
                        NpcID = npcID,
                        PlayerID = playerID,
                        QuestStepID = firstStep.ID
                    };
                    await questProgress.Save();

                    return firstStep;
                }
            }

            return null;
        }

        public static async Task<QuestStep> GetNextQuestStepForPlayer(string npcID, string playerID)
        {
            QuestStep currentStep = await GetCurrentQuestStepForPlayer(npcID, playerID);
            if(currentStep != null)
            {
                QuestStep nextStep = await Database.Instance.Connection.Table<QuestStep>().Where(x => x.NpcID == npcID && x.StepNumber > currentStep.StepNumber).OrderBy(x => x.StepNumber).FirstOrDefaultAsync();
                return nextStep;
            }

            return null;
        }

        public static async Task<int> Assert(QuestStep questStep)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(questStep);
        }

        public static async Task<int> Delete(QuestStep questStep)
        {
            return await Database.Instance.Connection.DeleteAsync(questStep);
        }

        public static async Task<int> DeleteAllForNPC(string npcID)
        {
            return await Database.Instance.Connection.Table<QuestStep>().DeleteAsync(x => x.NpcID == npcID);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<QuestStep>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is QuestStep))
                return false;

            return ((QuestStep)obj).ID == ID;
        }

        public override string ToString()
        {
            return Description;
        }
    }
}
