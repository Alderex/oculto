﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Utils
{
    public class Faction
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string Name { get; set; }

        public static async Task<Faction> Get(string ID)
        {
            return await Database.Instance.Connection.Table<Faction>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<Faction>> GetAll()
        {
            return await Database.Instance.Connection.Table<Faction>().OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<int> Assert(Faction faction)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(faction);
        }

        public static async Task<int> Delete(Faction faction)
        {
            return await Database.Instance.Connection.DeleteAsync(faction);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<Faction>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Faction))
                return false;

            return ((Faction)obj).ID == ID;
        }
    }
}
