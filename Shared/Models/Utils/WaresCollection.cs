﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Utils
{
    public class WaresCollection
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string Name { get; set; }

        public WaresCollection() { }

        public static async Task<WaresCollection> Get(string ID)
        {
            return await Database.Instance.Connection.Table<WaresCollection>().Where(i => i.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<WaresCollection>> GetAll()
        {
            return await Database.Instance.Connection.Table<WaresCollection>().OrderBy(w => w.Name).ToListAsync();
        }

        public static async Task<int> Assert(WaresCollection wares)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(wares);
        }

        public static async Task<int> Delete(WaresCollection wares)
        {
            return await Database.Instance.Connection.DeleteAsync(wares);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<WaresCollection>();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        public async Task<List<Definitions.Item>> GetItemsInCollection()
        {
            List<WaresEntry> wares = await Database.Instance.Connection.Table<WaresEntry>().Where(x => x.WaresCollectionID == ID).ToListAsync();
            List<Definitions.Item> items = new List<Definitions.Item>();
            foreach (WaresEntry ware in wares)
            {
                Definitions.Item item = await Definitions.Item.Get(ware.ItemID);
                items.Add(item);
            }
            return items.OrderBy(x => x.Name).ToList();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
