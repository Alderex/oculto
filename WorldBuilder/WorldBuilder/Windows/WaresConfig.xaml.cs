﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class WaresConfig : Window
    {
        private WaresCollection currentWaresCollection;
        private List<WaresEntry> waresEntries;

        public WaresConfig(WaresCollection waresCollection)
        {
            InitializeComponent();
            LoadData(waresCollection);
        }

        private async void LoadData(WaresCollection waresCollection)
        {
            List<Item> items = await Item.GetAll();

            itemList.ItemsSource = items;
            itemList.SelectionChanged += ItemList_SelectionChanged;

            currentWaresCollection = waresCollection;

            if (waresCollection != null)
            {
                if (waresCollection.Name != null)
                    name.Text = waresCollection.Name;
            }
            else
            {
                currentWaresCollection = new WaresCollection();
                currentWaresCollection.ID = Guid.NewGuid().ToString();
            }

            // Load the items for this selected wares list
            waresEntries = await WaresEntry.GetAllForCollection(currentWaresCollection.ID);

            foreach (WaresEntry entry in waresEntries)
                await entry.RefreshDetails();

            waresEntriesList.ItemsSource = waresEntries;
        }

        private void ItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (itemList.SelectedIndex != -1)
            {
                Item item = itemList.SelectedValue as Item;

                string id = Guid.NewGuid().ToString();
                WaresEntry waresEntry = new WaresEntry(id, currentWaresCollection.ID, item.ID);
                waresEntries.Add(waresEntry);
                waresEntriesList.Items.Refresh();
                itemList.SelectedIndex = -1;
            }
        }

        private void RemoveWares(object sender, RoutedEventArgs e)
        {
            WaresEntry waresEntry = ((Button)sender).DataContext as WaresEntry;
            if (waresEntry != null)
            {
                waresEntries.Remove(waresEntry);
                waresEntriesList.Items.Refresh();
            }
        }

        private async void SaveWaresCollection(object sender, RoutedEventArgs e)
        {
            currentWaresCollection.Name = name.Text;
            await currentWaresCollection.Save();

            // Delete our current entries for this collection and then save the ones in our list
            await WaresEntry.DeleteAllForCollection(currentWaresCollection.ID);
            foreach (WaresEntry entry in waresEntries)
                await entry.Save();

            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
