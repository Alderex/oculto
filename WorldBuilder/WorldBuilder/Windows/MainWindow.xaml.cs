﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WorldBuilder.Windows;
using Shared.Models.Definitions;
using Shared.Models.WorldStart;
using Shared.Models.Utils;

namespace WorldBuilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Shared.Database database;

        public MainWindow(string dbFilePath)
        {
            InitializeComponent();
            LoadData(dbFilePath);
        }

        private async void LoadData(string dbFilePath)
        {
            database = new Shared.Database(dbFilePath);
            await database.Init();

            ddAreaList.SelectionChanged += AreaList_SelectionChanged;

            Refresh();
        }

        private async void Refresh()
        {
            // Populating the room list is a little weird because we use the whole collection of rooms to generate an area list, and then use the area
            // list to filter our list of rooms
            List<Room> rooms = await Room.GetAll();
            List<string> areas = rooms.Select(r => r.Area).Distinct().OrderBy(a => a).ToList();

            object currentArea = null;
            if (ddAreaList.SelectedIndex != -1)
                currentArea = ddAreaList.SelectedValue;

            ddAreaList.ItemsSource = areas;
            if (currentArea != null)
                ddAreaList.SelectedValue = currentArea;
            else if (ddAreaList.Items.Count > 0)
                ddAreaList.SelectedIndex = 0;
            else
                ddAreaList.SelectedIndex = -1;

            // Filter our rooms based on our current area if we have one selected
            if(currentArea != null)
                rooms = rooms.Where(r => r.Area == currentArea.ToString()).ToList();
            lvRooms.ItemsSource = rooms;

            List<NPC> npcs = await NPC.GetAll();
            npcs = npcs.OrderBy(n => n.Name).ToList();
            lvNPCs.ItemsSource = npcs;

            DisplayAndFilterItems();

            List<Spell> spells = await Spell.GetAll();
            lvSpells.ItemsSource = spells;

            List<LootTable> lootTables = await LootTable.GetAll();
            lvLootTables.ItemsSource = lootTables;

            List<StatBlock> statBlocks = await StatBlock.GetAll();
            lvStatBlocks.ItemsSource = statBlocks;

            List<WaresCollection> wares = await WaresCollection.GetAll();
            lvWares.ItemsSource = wares;

            List<Trap> traps = await Trap.GetAll();
            lvTraps.ItemsSource = traps;
        }

        #region Menu Item Methods

        private void NewFaction(object sender, RoutedEventArgs e)
        {
            FactionConfig newFaction = new FactionConfig(null);
            newFaction.SizeToContent = SizeToContent.Manual;
            newFaction.Show();
        }

        private void ManageFactions(object sender, RoutedEventArgs e)
        {
            FactionList factionList = new FactionList();
            factionList.SizeToContent = SizeToContent.Manual;
            factionList.Show();
        }

        private void NewClass(object sender, RoutedEventArgs e)
        {
            PlayerTemplateConfig playerTemplateConfig = new PlayerTemplateConfig(null);
            playerTemplateConfig.SizeToContent = SizeToContent.Manual;
            playerTemplateConfig.Show();
        }

        private void ManageClasses(object sender, RoutedEventArgs e)
        {
            PlayerTemplateList playerTemplateList = new PlayerTemplateList();
            playerTemplateList.SizeToContent = SizeToContent.Manual;
            playerTemplateList.Show();
        }

        #endregion

        #region Room Methods

        private void AreaList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Refresh();
        }

        private void NewRoom(object sender, RoutedEventArgs e)
        {
            RoomConfig newRoom = new RoomConfig(null);
            newRoom.SizeToContent = SizeToContent.Manual;
            newRoom.Show();
            newRoom.Closed += Window_Closed;
        }

        private void EditRoom(object sender, RoutedEventArgs e)
        {
            Room room = ((Button)sender).DataContext as Room;

            RoomConfig roomConfig = new RoomConfig(room);
            roomConfig.SizeToContent = SizeToContent.Manual;
            roomConfig.Show();
            roomConfig.Closed += Window_Closed;
        }

        private async void DeleteRoom(object sender, RoutedEventArgs e)
        {
            Room room = ((Button)sender).DataContext as Room;

            // Delete all the doors, and instanced NPCs and items for this room
            await Door.DeleteAllInRoom(room);
            await StartingItem.DeleteAllForRoom(room);
            await StartingNPC.DeleteAllForRoom(room);
            
            await Room.Delete(room);
            Refresh();
        }

        #endregion

        #region NPC Methods

        private void NewNPC(object sender, RoutedEventArgs e)
        {
            NpcConfig npcConfig = new NpcConfig(null);
            npcConfig.SizeToContent = SizeToContent.WidthAndHeight;
            npcConfig.Show();
            npcConfig.Closed += Window_Closed;
        }

        private void DuplicateNPC(object sender, RoutedEventArgs e)
        {
            NPC npc = ((Button)sender).DataContext as NPC;

            npc.ID = Guid.NewGuid().ToString();

            NpcConfig npcConfig = new NpcConfig(npc);
            npcConfig.SizeToContent = SizeToContent.WidthAndHeight;
            npcConfig.Show();
            npcConfig.Closed += Window_Closed;
        }

        private void EditNPC(object sender, RoutedEventArgs e)
        {
            NPC npc = ((Button)sender).DataContext as NPC;

            NpcConfig npcConfig = new NpcConfig(npc);
            npcConfig.SizeToContent = SizeToContent.WidthAndHeight;
            npcConfig.Show();
            npcConfig.Closed += Window_Closed;
        }

        private async void DeleteNPC(object sender, RoutedEventArgs e)
        {
            NPC npc = ((Button)sender).DataContext as NPC;

            // Delete any starting NPCs that has this NPC as their source
            List<StartingNPC> startingNPCs = await StartingNPC.GetAll();
            List<StartingNPC> affectedNPCs = startingNPCs.Where(s => s.SourceID == npc.ID).ToList();
            foreach (StartingNPC affectedNPC in affectedNPCs)
                await StartingNPC.Delete(affectedNPC);

            await NPC.Delete(npc);
            Refresh();
        }

        #endregion

        #region Item Methods
        private void ItemSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            DisplayAndFilterItems();
        }

        private async void DisplayAndFilterItems()
        {
            List<Item> items = await Item.GetAll();
            if (tbItemSearch.Text != "")
            {
                items = items.Where(x => x.Name.Contains(tbItemSearch.Text)).ToList();
            }
            lvItems.ItemsSource = items;
        }

        private void NewItem(object sender, RoutedEventArgs e)
        {
            ItemConfig itemConfig = new ItemConfig(null);
            itemConfig.SizeToContent = SizeToContent.Manual;
            itemConfig.Show();
            itemConfig.Closed += Window_Closed;
        }

        private void EditItem(object sender, RoutedEventArgs e)
        {
            Item item = ((Button)sender).DataContext as Item;

            ItemConfig itemConfig = new ItemConfig(item);
            itemConfig.SizeToContent = SizeToContent.Manual;
            itemConfig.Show();
            itemConfig.Closed += Window_Closed;
        }

        private async void DeleteItem(object sender, RoutedEventArgs e)
        {
            Item item = ((Button)sender).DataContext as Item;

            // Delete any starting items that has this item as their source
            List<StartingItem> startingItems = await StartingItem.GetAll();
            List<StartingItem> affectedItems = startingItems.Where(i => i.SourceID == item.ID).ToList();
            foreach (StartingItem affectedItem in affectedItems)
                await StartingItem.Delete(affectedItem);

            await Item.Delete(item);
            Refresh();
        }

        #endregion

        #region Loot Table Methods

        private void NewLootTable(object sender, RoutedEventArgs e)
        {
            LootTableConfig lootTableConfig = new LootTableConfig(null);
            lootTableConfig.SizeToContent = SizeToContent.WidthAndHeight;
            lootTableConfig.Show();
            lootTableConfig.Closed += Window_Closed;
        }

        private void EditLootTable(object sender, RoutedEventArgs e)
        {
            LootTable lootTable = ((Button)sender).DataContext as LootTable;

            LootTableConfig lootTableConfig = new LootTableConfig(lootTable);
            lootTableConfig.SizeToContent = SizeToContent.WidthAndHeight;
            lootTableConfig.Show();
            lootTableConfig.Closed += Window_Closed;
        }

        private async void DeleteLootTable(object sender, RoutedEventArgs e)
        {
            LootTable lootTable = ((Button)sender).DataContext as LootTable;
            await LootTable.Delete(lootTable);
            Refresh();
        }

        #endregion

        #region Wares Methods

        private void NewWares(object sender, RoutedEventArgs e)
        {
            WaresConfig waresConfig = new WaresConfig(null);
            waresConfig.SizeToContent = SizeToContent.WidthAndHeight;
            waresConfig.Show();
            waresConfig.Closed += Window_Closed;
        }

        private void EditWares(object sender, RoutedEventArgs e)
        {
            WaresCollection waresCollection = ((Button)sender).DataContext as WaresCollection;

            WaresConfig waresConfig = new WaresConfig(waresCollection);
            waresConfig.SizeToContent = SizeToContent.WidthAndHeight;
            waresConfig.Show();
            waresConfig.Closed += Window_Closed;
        }

        private async void DeleteWares(object sender, RoutedEventArgs e)
        {
            WaresCollection waresCollection = ((Button)sender).DataContext as WaresCollection;
            await WaresCollection.Delete(waresCollection);
            Refresh();
        }

        #endregion

        #region Spell Methods

        private void NewSpell(object sender, RoutedEventArgs e)
        {
            SpellConfig spellConfig = new SpellConfig(null);
            spellConfig.SizeToContent = SizeToContent.WidthAndHeight;
            spellConfig.Show();
            spellConfig.Closed += Window_Closed;
        }

        private void EditSpell(object sender, RoutedEventArgs e)
        {
            Spell spell = ((Button)sender).DataContext as Spell;

            SpellConfig spellConfig = new SpellConfig(spell);
            spellConfig.SizeToContent = SizeToContent.WidthAndHeight;
            spellConfig.Show();
            spellConfig.Closed += Window_Closed;
        }

        private async void DeleteSpell(object sender, RoutedEventArgs e)
        {
            Spell spell = ((Button)sender).DataContext as Spell;
            await Spell.Delete(spell);
            Refresh();
        }

        #endregion

        #region Stat Block Methods

        private void NewStatBlock(object sender, RoutedEventArgs e)
        {
            StatBlockConfig statBlockConfig = new StatBlockConfig(null);
            statBlockConfig.SizeToContent = SizeToContent.Manual;
            statBlockConfig.Show();
            statBlockConfig.Closed += Window_Closed;
        }

        private void EditStatBlock(object sender, RoutedEventArgs e)
        {
            StatBlock statBlock = ((Button)sender).DataContext as StatBlock;

            StatBlockConfig statBlockConfig = new StatBlockConfig(statBlock);
            statBlockConfig.SizeToContent = SizeToContent.Manual;
            statBlockConfig.Show();
            statBlockConfig.Closed += Window_Closed;
        }

        private async void DeleteStatBlock(object sender, RoutedEventArgs e)
        {
            StatBlock statBlock = ((Button)sender).DataContext as StatBlock;
            await StatBlock.Delete(statBlock);
            Refresh();
        }

        #endregion

        #region Trap Methods

        private void NewTrap(object sender, RoutedEventArgs e)
        {
            TrapConfig trapConfig = new TrapConfig(null);
            trapConfig.SizeToContent = SizeToContent.WidthAndHeight;
            trapConfig.Show();
            trapConfig.Closed += Window_Closed;
        }

        private void EditTrap(object sender, RoutedEventArgs e)
        {
            Trap trap = ((Button)sender).DataContext as Trap;

            TrapConfig trapConfig = new TrapConfig(trap);
            trapConfig.SizeToContent = SizeToContent.WidthAndHeight;
            trapConfig.Show();
            trapConfig.Closed += Window_Closed;
        }

        private async void DeleteTrap(object sender, RoutedEventArgs e)
        {
            Trap trap = ((Button)sender).DataContext as Trap;
            await Trap.Delete(trap);
            Refresh();
        }

        #endregion

        private void Window_Closed(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}
