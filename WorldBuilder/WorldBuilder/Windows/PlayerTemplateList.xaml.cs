﻿using Shared.Models.Definitions;
using Shared.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for FactionList.xaml
    /// </summary>
    public partial class PlayerTemplateList : Window
    {
        public PlayerTemplateList()
        {
            InitializeComponent();
            _ = RefreshList();
        }

        private async Task RefreshList()
        {
            List<Player> playerTemplates = await Player.GetAll();
            lvTemplates.ItemsSource = playerTemplates;
        }

        private void NewTemplate(object sender, RoutedEventArgs e)
        {
            PlayerTemplateConfig newPlayerTemplate = new PlayerTemplateConfig(null);
            newPlayerTemplate.SizeToContent = SizeToContent.Manual;
            newPlayerTemplate.Show();
            newPlayerTemplate.Closed += NewTemplate_Closed;
        }

        private void NewTemplate_Closed(object sender, EventArgs e)
        {
            _ = RefreshList();
        }

        private void EditTemplate(object sender, RoutedEventArgs e)
        {
            Player playerTemplate = ((Button)sender).DataContext as Player;

            PlayerTemplateConfig playerTemplateConfig = new PlayerTemplateConfig(playerTemplate);
            playerTemplateConfig.SizeToContent = SizeToContent.Manual;
            playerTemplateConfig.Show();
            playerTemplateConfig.Closed += NewTemplate_Closed;
        }

        private async void DeleteTemplate(object sender, RoutedEventArgs e)
        {
            Player playerTemplate = ((Button)sender).DataContext as Player;
            await Player.Delete(playerTemplate);
            await RefreshList();
        }
    }
}
