﻿using Shared.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for FactionList.xaml
    /// </summary>
    public partial class FactionList : Window
    {
        public FactionList()
        {
            InitializeComponent();
            _ = RefreshList();
        }

        private async Task RefreshList()
        {
            List<Faction> factions = await Faction.GetAll();
            lvFactions.ItemsSource = factions;
        }

        private void NewFaction(object sender, RoutedEventArgs e)
        {
            FactionConfig newFaction = new FactionConfig(null);
            newFaction.SizeToContent = SizeToContent.Manual;
            newFaction.Show();
            newFaction.Closed += NewFaction_Closed;
        }

        private void NewFaction_Closed(object sender, EventArgs e)
        {
            _ = RefreshList();
        }

        private void EditFaction(object sender, RoutedEventArgs e)
        {
            Faction faction = ((Button)sender).DataContext as Faction;

            FactionConfig factionConfig = new FactionConfig(faction);
            factionConfig.SizeToContent = SizeToContent.Manual;
            factionConfig.Show();
            factionConfig.Closed += NewFaction_Closed;
        }

        private async void DeleteFaction(object sender, RoutedEventArgs e)
        {
            Faction faction = ((Button)sender).DataContext as Faction;
            await Faction.Delete(faction);
            await RefreshList();
        }
    }
}
