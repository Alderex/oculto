﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class SpellConfig : Window
    {
        private Spell currentSpell;

        public SpellConfig(Spell spell)
        {
            InitializeComponent();

            cbSpellType.Items.Add("Damage");
            cbSpellType.Items.Add("Heal");
            cbSpellType.Items.Add("Identify");

            currentSpell = spell;

            if (spell != null)
            {
                if (spell.Name != null)
                    tbName.Text = spell.Name;
                if (spell.Description != null)
                    tbDescription.Text = spell.Description;
                if (spell.OnUseOutput != null)
                    tbOnUseOutput.Text = spell.OnUseOutput;

                tbManaCost.Text = spell.ManaCost.ToString();
                cbSpellType.SelectedIndex = spell.SpellType;
                tbAmount.Text = spell.Amount.ToString();
            }
            else
            {
                currentSpell = new Spell();
                currentSpell.ID = Guid.NewGuid().ToString();

                cbSpellType.SelectedIndex = 0;
            }
        }

        private async void SaveSpell(object sender, RoutedEventArgs e)
        {
            currentSpell.Name = tbName.Text;
            currentSpell.Description = tbDescription.Text;
            currentSpell.OnUseOutput = tbOnUseOutput.Text;
            currentSpell.SpellType = cbSpellType.SelectedIndex;
            if (tbManaCost.Text != "")
                currentSpell.ManaCost = Int32.Parse(tbManaCost.Text);
            if (tbAmount.Text != "")
                currentSpell.Amount = Int32.Parse(tbAmount.Text);

            await currentSpell.Save();
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
