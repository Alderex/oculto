﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Shared.Models.Definitions;
using Shared.Models.WorldStart;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NewRoom.xaml
    /// </summary>
    public partial class RoomConfig : Window
    {
        private Room currentRoom;

        private List<Door> doors;
        private List<StartingItem> startingItems;
        private List<StartingNPC> startingNPCs;

        public RoomConfig(Room room)
        {
            InitializeComponent();
            _ = LoadData(room);
        }

        private async Task LoadData(Room room)
        {
            currentRoom = room;
            if (currentRoom != null)
            {
                tbName.Text = room.Name;
                tbArea.Text = room.Area;
                tbFlavor.Text = room.Description;
            }
            else
            {
                currentRoom = new Room();
                currentRoom.ID = Guid.NewGuid().ToString();
            }

            List<Room> rooms = await Room.GetAll();
            ddDoors.ItemsSource = rooms;

            List<NPC> npcs = await NPC.GetAll();
            ddNPCs.ItemsSource = npcs;

            List<Item> items = await Item.GetAll();
            ddItems.ItemsSource = items;

            // Load our doors, NPCs, and items
            doors = await Door.GetDoorsInRoom(currentRoom);
            lvDoorList.ItemsSource = doors;

            startingNPCs = await StartingNPC.GetStartingNPCsForRoom(currentRoom);
            foreach (StartingNPC npc in startingNPCs)
                await npc.RefreshDetails();
            lvStartingNPCs.ItemsSource = startingNPCs;

            startingItems = await StartingItem.GetStartingItemsInRoom(currentRoom);
            foreach (StartingItem item in startingItems)
                await item.RefreshDetails();
            lvStartingItems.ItemsSource = startingItems;

            ddItems.SelectionChanged += Items_SelectionChanged;
            ddNPCs.SelectionChanged += NPCs_SelectionChanged;
            ddDoors.SelectionChanged += Doors_SelectionChanged;

            tbRoomID.Text = "ID: " + currentRoom.ID;
            save.Click += SaveRoom;
        }

        private void Doors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ddDoors.SelectedIndex != -1)
            {
                Room room = ddDoors.SelectedValue as Room;

                Door door = new Door();
                door.ID = Guid.NewGuid().ToString();
                door.RoomID = currentRoom.ID;
                door.Name = "Temp Door Name";
                door.RoomDestination = room.ID;
                doors.Add(door);

                lvDoorList.Items.Refresh();

                ddDoors.SelectedIndex = -1;
            }
        }

        private void NPCs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ddNPCs.SelectedIndex != -1)
            {
                NPC npc = ddNPCs.SelectedValue as NPC;
                string id = Guid.NewGuid().ToString();

                StartingNPC startingNPC = new StartingNPC(id, npc.ID, currentRoom.ID);
                startingNPCs.Add(startingNPC);

                lvStartingNPCs.Items.Refresh();

                ddNPCs.SelectedIndex = -1;
            }
        }

        private void Items_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (ddItems.SelectedIndex != -1)
            {
                Item item = ddItems.SelectedValue as Item;
                string id = Guid.NewGuid().ToString();

                StartingItem startingItem = new StartingItem(id, item.ID, currentRoom.ID);
                startingItems.Add(startingItem);

                lvStartingItems.Items.Refresh();

                ddItems.SelectedIndex = -1;
            }
        }

        public void UpdateDoor(Door door)
        {
            // We opened up the config for a door, and now we need to update our local copy of it so we can save it into the database when we save our room
            int index = doors.FindIndex(d => d.ID.Equals(door.ID));
            if (index != -1)
                doors[index] = door;
            lvDoorList.Items.Refresh();
        }

        private void EditDoor(object sender, RoutedEventArgs e)
        {
            Door door = ((Button)sender).DataContext as Door;

            DoorConfig doorConfig = new DoorConfig(this, door);
            doorConfig.SizeToContent = SizeToContent.Manual;
            doorConfig.Show();
        }

        private void DeleteDoor(object sender, RoutedEventArgs e)
        {
            Door door = ((Button)sender).DataContext as Door;

            doors.Remove(door);
            lvDoorList.Items.Refresh();
        }

        private void RemoveNPC(object sender, RoutedEventArgs e)
        {
            StartingNPC startingNPC = ((Button)sender).DataContext as StartingNPC;
            startingNPCs.Remove(startingNPC);
            lvStartingNPCs.Items.Refresh();
        }

        private void RemoveItem(object sender, RoutedEventArgs e)
        {
            StartingItem startingItem = ((Button)sender).DataContext as StartingItem;
            startingItems.Remove(startingItem);
            lvStartingItems.Items.Refresh();
        }

        private async void SaveRoom(object sender, RoutedEventArgs e)
        {
            currentRoom.Name = tbName.Text;
            currentRoom.Area = tbArea.Text;
            currentRoom.Description = tbFlavor.Text;
            await currentRoom.Save();

            await Door.DeleteAllInRoom(currentRoom);
            foreach (Door door in doors)
                await door.Save();

            await StartingItem.DeleteAllForRoom(currentRoom);
            foreach (StartingItem item in startingItems)
                await item.Save();

            await StartingNPC.DeleteAllForRoom(currentRoom);
            foreach (StartingNPC npc in startingNPCs)
                await npc.Save();

            Close();
        }
    }
}
