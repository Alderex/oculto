﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class DoorConfig : Window
    {
        private Door currentDoor;
        private RoomConfig roomConfig;

        public DoorConfig(RoomConfig roomConfig, Door door)
        {
            InitializeComponent();
            this.roomConfig = roomConfig;
            roomConfig.Closed += RoomConfig_Closed;
            LoadData(door);
        }

        private void RoomConfig_Closed(object sender, EventArgs e)
        {
            // If our room edit window closes, close this window as well
            // We only save our doors to the database by saving our room
            Close();
        }

        private async void LoadData(Door door)
        {
            currentDoor = door;

            List<Room> rooms = await Room.GetAll();
            rooms.OrderBy(x => x.Area);
            ddRoomList.ItemsSource = rooms;

            List<Item> items = await Item.GetAll();
            ddItemList.ItemsSource = items;

            List<Trap> traps = await Trap.GetAll();
            ddTrapList.ItemsSource = traps;

            if (currentDoor.Name != null)
                tbName.Text = currentDoor.Name;
            if (currentDoor.Description != null)
                tbDescription.Text = currentDoor.Description;
            if (currentDoor.OnUseOutput != null)
                tbOnUseOutput.Text = currentDoor.OnUseOutput;
            tbPerceptionReq.Text = currentDoor.PerceptionRequired.ToString();

            if(door.RoomDestination != null)
            {
                Room room = await Room.Get(door.RoomDestination);
                ddRoomList.SelectedItem = room;
            }

            if (door.RequiredKey != null)
            {
                Item item = await Item.Get(door.RequiredKey);
                ddItemList.SelectedItem = item;
            }

            if(door.TrapID != null)
            {
                Trap trap = await Trap.Get(door.TrapID);
                ddTrapList.SelectedItem = trap;
            }
        }

        private void RemoveKey(object sender, RoutedEventArgs e)
        {
            ddItemList.SelectedIndex = -1;
        }

        private void RemoveTrap(object sender, RoutedEventArgs e)
        {
            ddTrapList.SelectedIndex = -1;
        }

        private void SaveDoor(object sender, RoutedEventArgs e)
        {
            if(roomConfig != null)
            {
                currentDoor.Name = tbName.Text;
                currentDoor.Description = tbDescription.Text;
                currentDoor.OnUseOutput = tbOnUseOutput.Text;
                if (tbPerceptionReq.Text != "")
                    currentDoor.PerceptionRequired = Int32.Parse(tbPerceptionReq.Text);

                if(ddItemList.SelectedIndex != -1)
                {
                    Item key = ddItemList.SelectedValue as Item;
                    currentDoor.RequiredKey = key.ID;
                }
                else
                {
                    currentDoor.RequiredKey = null;
                }

                if (ddTrapList.SelectedIndex != -1)
                {
                    Trap trap = ddTrapList.SelectedValue as Trap;
                    currentDoor.TrapID = trap.ID;
                }
                else
                {
                    currentDoor.TrapID = null;
                }

                if (ddRoomList.SelectedIndex != -1)
                {
                    Room room = ddRoomList.SelectedValue as Room;
                    currentDoor.RoomDestination = room.ID;
                }

                roomConfig.UpdateDoor(currentDoor);
                Close();
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
