﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class FactionConfig : Window
    {
        private Faction currentFaction;

        public FactionConfig(Faction faction)
        {
            InitializeComponent();

            currentFaction = faction;

            if (faction != null)
            {
                if (faction.Name != null)
                    name.Text = faction.Name;
            }
            else
            {
                currentFaction = new Faction();
                currentFaction.ID = Guid.NewGuid().ToString();
            }
        }

        private async void SaveFaction(object sender, RoutedEventArgs e)
        {
            currentFaction.Name = name.Text;

            await currentFaction.Save();
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
