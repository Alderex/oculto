﻿using System;
using System.Windows;
using System.Windows.Input;
using Shared.Models.Definitions;
using System.Text.RegularExpressions;
using Shared.Models.WorldStart;
using System.Collections.Generic;
using System.Windows.Controls;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for ItemConfig.xaml
    /// </summary>
    public partial class PlayerTemplateConfig : Window
    {
        private Player currentPlayerTemplate;
        private List<StartingInventory> startingInventoryItems;

        public PlayerTemplateConfig(Player playerTemplate)
        {
            InitializeComponent();
            LoadData(playerTemplate);
        }

        private async void LoadData(Player playerTemplate)
        {
            currentPlayerTemplate = playerTemplate;

            List<Item> items = await Item.GetAll();

            itemList.ItemsSource = items;
            itemList.SelectionChanged += ItemList_SelectionChanged;

            if (playerTemplate != null)
            {
                if (playerTemplate.Name != null)
                    name.Text = playerTemplate.Name;

                tbStrength.Text = playerTemplate.Strength.ToString();
                tbDexterity.Text = playerTemplate.Dexterity.ToString();
                tbIntelligence.Text = playerTemplate.Intelligence.ToString();
                tbWisdom.Text = playerTemplate.Wisdom.ToString();
                tbCharisma.Text = playerTemplate.Charisma.ToString();
                tbConstitution.Text = playerTemplate.Constitution.ToString();

                tbGold.Text = playerTemplate.Gold.ToString();
            }
            else
            {
                currentPlayerTemplate = new Player();
                currentPlayerTemplate.ID = Guid.NewGuid().ToString();
            }

            startingInventoryItems = await StartingInventory.GetStartingInventoryForPlayer(currentPlayerTemplate.ID);

            foreach (StartingInventory item in startingInventoryItems)
                await item.RefreshDetails();

            lvStartingItemsList.ItemsSource = startingInventoryItems;
        }

        private async void ItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (itemList.SelectedIndex != -1 && itemList.IsDropDownOpen)
            {
                Item item = itemList.SelectedValue as Item;

                string id = Guid.NewGuid().ToString();
                StartingInventory startingInventoryEntry = new StartingInventory(id, item.ID, currentPlayerTemplate.ID);
                await startingInventoryEntry.RefreshDetails();
                startingInventoryItems.Add(startingInventoryEntry);
                lvStartingItemsList.Items.Refresh();
                itemList.SelectedIndex = -1;
            }
        }

        private void RemoveStartingInventoryItem(object sender, RoutedEventArgs e)
        {
            StartingInventory startingInventoryItem = ((Button)sender).DataContext as StartingInventory;
            if (startingInventoryItem != null)
            {
                startingInventoryItems.Remove(startingInventoryItem);
                lvStartingItemsList.Items.Refresh();
            }
        }

        private async void SaveTemplate(object sender, RoutedEventArgs e)
        {
            currentPlayerTemplate.Name = name.Text;
            if (tbStrength.Text != "")
                currentPlayerTemplate.Strength = Int32.Parse(tbStrength.Text);
            if (tbDexterity.Text != "")
                currentPlayerTemplate.Dexterity = Int32.Parse(tbDexterity.Text);
            if (tbIntelligence.Text != "")
                currentPlayerTemplate.Intelligence = Int32.Parse(tbIntelligence.Text);
            if (tbWisdom.Text != "")
                currentPlayerTemplate.Wisdom = Int32.Parse(tbWisdom.Text);
            if (tbCharisma.Text != "")
                currentPlayerTemplate.Charisma = Int32.Parse(tbCharisma.Text);
            if (tbConstitution.Text != "")
                currentPlayerTemplate.Constitution = Int32.Parse(tbConstitution.Text);

            if (tbGold.Text != "")
                currentPlayerTemplate.Gold = Int32.Parse(tbGold.Text);

            // Delete our current entries for this collection and then save the ones in our list
            await StartingInventory.DeleteAllForPlayer(currentPlayerTemplate.ID);
            foreach (StartingInventory startingInventoryItem in startingInventoryItems)
                await startingInventoryItem.Save();

            await Player.Assert(currentPlayerTemplate);

            Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
