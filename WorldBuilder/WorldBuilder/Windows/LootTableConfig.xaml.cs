﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class LootTableConfig : Window
    {
        private LootTable currentLootTable;
        private List<LootTableEntry> lootTableEntries;

        public LootTableConfig(LootTable lootTable)
        {
            InitializeComponent();
            LoadData(lootTable);
        }

        private async void LoadData(LootTable lootTable)
        {
            List<Item> items = await Item.GetAll();
            lootList.ItemsSource = items;
            currentLootTable = lootTable;

            if (lootTable != null)
            {
                if (lootTable.Name != null)
                    name.Text = lootTable.Name;
            }
            else
            {
                currentLootTable = new LootTable();
                currentLootTable.ID = Guid.NewGuid().ToString();
            }

            // Load the loot for this selected loot table
            lootTableEntries = await LootTableEntry.GetAllForTable(currentLootTable.ID);

            // Refresh the item names for each of the entries
            foreach (LootTableEntry entry in lootTableEntries)
                await entry.RefreshDetails();

            lootTableEntriesList.ItemsSource = lootTableEntries;
        }

        private void AddLoot(object sender, RoutedEventArgs e)
        {
            if (lootList.SelectedIndex != -1)
            {
                Item item = lootList.SelectedValue as Item;

                string id = Guid.NewGuid().ToString();
                LootTableEntry lootEntry = new LootTableEntry(id, currentLootTable.ID, item.ID, Convert.ToInt32(dropChance.Text));
                lootTableEntries.Add(lootEntry);
                lootTableEntriesList.Items.Refresh();

                lootList.SelectedIndex = -1;
            }
        }

        private void RemoveLoot(object sender, RoutedEventArgs e)
        {
            LootTableEntry lootEntry = ((Button)sender).DataContext as LootTableEntry;
            if (lootEntry != null)
            {
                lootTableEntries.Remove(lootEntry);
                lootTableEntriesList.Items.Refresh();
            }
        }

        private async void SaveLootTable(object sender, RoutedEventArgs e)
        {
            currentLootTable.Name = name.Text;
            await currentLootTable.Save();

            // Delete our current entries for this table and then save the ones in our list
            await LootTableEntry.DeleteAllForTable(currentLootTable.ID);
            foreach (LootTableEntry entry in lootTableEntries)
                await entry.Save();

            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
