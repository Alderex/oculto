﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for SetupWindow.xaml
    /// </summary>
    public partial class SetupWindow : Window
    {
        public SetupWindow()
        {
            InitializeComponent();
        }

        private void BtnSetWorld_Click(object sender, RoutedEventArgs e)
        {/*
#if DEBUG
            MainWindow mainWindow = new MainWindow("C:\\Users\\Alderex\\Documents\\world.db3");
            mainWindow.SizeToContent = SizeToContent.Manual;
            mainWindow.Show();
            Close();
#else*/
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Set World Path";
            openFileDialog.Filter = "SQLite World File (*.db3)|*.db3";
            if (openFileDialog.ShowDialog() == true)
            {
                MainWindow mainWindow = new MainWindow(openFileDialog.FileName);
                mainWindow.SizeToContent = SizeToContent.Manual;
                mainWindow.Show();
                Close();
            }
//#endif
        }
    }
}
