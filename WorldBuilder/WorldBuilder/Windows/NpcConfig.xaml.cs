﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class NpcConfig : Window
    {
        private NPC currentNPC;
        private List<QuestStep> questSteps;

        public NpcConfig(NPC npc)
        {
            InitializeComponent();
            LoadData(npc);
        }

        private async void LoadData(NPC npc)
        {
            List<WaresCollection> waresCollections = await WaresCollection.GetAll();
            waresList.ItemsSource = waresCollections;

            List<LootTable> lootTables = await LootTable.GetAll();
            lootTableList.ItemsSource = lootTables;

            List<StatBlock> statBlocks = await StatBlock.GetAll();
            statBlockList.ItemsSource = statBlocks;

            currentNPC = npc;

            if (npc != null)
            {
                if (npc.Name != null)
                    tbName.Text = npc.Name;
                if (npc.Description != null)
                    tbDescription.Text = npc.Description;
                if (npc.IntroPhrase != null)
                    tbIntroPhrase.Text = npc.IntroPhrase;
                if (npc.WaresRejectionPhrase != null)
                    tbWaresRejection.Text = npc.WaresRejectionPhrase;

                tbWaresRepReq.Text = npc.WaresRepRequirement.ToString();
                tbRespawnTimer.Text = npc.RespawnTimer.ToString();

                if (npc.LootTableID != null)
                {
                    LootTable lootTable = lootTables.Where(x => x.ID == npc.LootTableID).FirstOrDefault();
                    if (lootTable != null)
                    {
                        lootTableList.SelectedValue = lootTable;
                    }
                }
                if (npc.WaresCollectionID != null)
                {
                    WaresCollection wares = waresCollections.Where(x => x.ID == npc.WaresCollectionID).FirstOrDefault();
                    if (wares != null)
                    {
                        waresList.SelectedValue = wares;
                    }
                }
                if (npc.StatBlockID != null)
                {
                    StatBlock statBlock = statBlocks.Where(x => x.ID == npc.StatBlockID).FirstOrDefault();
                    if (statBlock != null)
                    {
                        statBlockList.SelectedValue = statBlock;
                    }
                }

                questSteps = await QuestStep.GetAllForNPC(npc.ID);
                lvQuestSteps.ItemsSource = questSteps;
            }
            else
            {
                currentNPC = new NPC();
                questSteps = new List<QuestStep>();
                currentNPC.ID = Guid.NewGuid().ToString();
            }
        }

        private void AddQuestStep(object sender, RoutedEventArgs e)
        {
            int nextStepNumber = 1;
            if (questSteps.Count > 0)
            {
                QuestStep lastStep = questSteps.OrderByDescending(x => x.StepNumber).FirstOrDefault();
                nextStepNumber = lastStep.StepNumber + 1;
            }

            QuestStep questStep = new QuestStep();
            questStep.ID = Guid.NewGuid().ToString();
            questStep.NpcID = currentNPC.ID;
            questStep.StepNumber = nextStepNumber;

            QuestStepConfig questStepConfig = new QuestStepConfig(this, questStep);
            questStepConfig.SizeToContent = SizeToContent.Manual;
            questStepConfig.Show();

            questSteps.Add(questStep);
            lvQuestSteps.Items.Refresh();
        }

        private void EditQuestStep(object sender, RoutedEventArgs e)
        {
            QuestStep questStep = ((Button)sender).DataContext as QuestStep;

            QuestStepConfig questStepConfig = new QuestStepConfig(this, questStep);
            questStepConfig.SizeToContent = SizeToContent.Manual;
            questStepConfig.Show();
        }

        public void UpdateQuestStep(QuestStep questStep)
        {
            // We opened up the config for a quest step, and now we need to update our local copy of it so we can save it into the database when we save our NPC
            int index = questSteps.FindIndex(q => q.ID.Equals(questStep.ID));
            if (index != -1)
                questSteps[index] = questStep;
            lvQuestSteps.Items.Refresh();
        }

        private void DeleteQuestStep(object sender, RoutedEventArgs e)
        {
            QuestStep questStep = ((Button)sender).DataContext as QuestStep;
            questSteps.Remove(questStep);
            lvQuestSteps.Items.Refresh();
        }

        private async void SaveNPC(object sender, RoutedEventArgs e)
        {
            currentNPC.Name = tbName.Text;
            currentNPC.Description = tbDescription.Text;
            currentNPC.IntroPhrase = tbIntroPhrase.Text;
            currentNPC.WaresRejectionPhrase = tbWaresRejection.Text;

            if (tbWaresRepReq.Text != "")
                currentNPC.WaresRepRequirement = Int32.Parse(tbWaresRepReq.Text);

            if (tbWaresRepReq.Text != "")
                currentNPC.RespawnTimer = Int32.Parse(tbRespawnTimer.Text);

            StatBlock statBlock = statBlockList.SelectedValue as StatBlock;
            if (statBlock != null)
                currentNPC.StatBlockID = statBlock.ID;

            WaresCollection wares = waresList.SelectedItem as WaresCollection;
            if (wares != null)
                currentNPC.WaresCollectionID = wares.ID;

            LootTable lootTable = lootTableList.SelectedValue as LootTable;
            if (lootTable != null)
                currentNPC.LootTableID = lootTable.ID;

            // Delete our current quest steps and rewrite them
            await QuestStep.DeleteAllForNPC(currentNPC.ID);
            foreach (QuestStep questStep in questSteps)
                await questStep.Save();

            await currentNPC.Save();

            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
