﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class QuestStepConfig : Window
    {
        private QuestStep currentQuestStep;
        private NpcConfig npcConfig;

        public QuestStepConfig(NpcConfig npcConfig, QuestStep questStep)
        {
            InitializeComponent();
            npcConfig.Closed += NpcConfig_Closed;
            LoadData(npcConfig, questStep);
        }

        private void NpcConfig_Closed(object sender, EventArgs e)
        {
            // If the parent NPC config window closes, close this window because we can't save our
            // Quest Step to the db anyway
            Close();
        }

        private async void LoadData(NpcConfig npcConfig, QuestStep questStep)
        {
            List<Item> items = await Item.GetAll();
            wantedItem.ItemsSource = items;
            rewardItem.ItemsSource = items;

            this.npcConfig = npcConfig;
            currentQuestStep = questStep;

            if (questStep != null)
            {
                tbStepNumber.Text = questStep.StepNumber.ToString();
                if (questStep.Description != null)
                    tbDescription.Text = questStep.Description;
                if (questStep.RepReqRejectionPhrase != null)
                    tbRepRejectionPhrase.Text = questStep.RepReqRejectionPhrase;
                if (questStep.StartPhrase != null)
                    tbStartPhase.Text = questStep.StartPhrase;
                if (questStep.RewardPhrase != null)
                    tbRewardPhrase.Text = questStep.RewardPhrase;
                if (questStep.RejectionPhrase != null)
                    tbRejectionPhrase.Text = questStep.RejectionPhrase;
                tbRepReq.Text = questStep.RepRequirement.ToString();
                tbRewardGold.Text = questStep.RewardGold.ToString();
                tbRepReward.Text = questStep.RepReward.ToString();
                tbRewardXP.Text = questStep.RewardXP.ToString();

                if (questStep.WantedItem != null)
                {
                    Item item = items.Where(x => x.ID == questStep.WantedItem).FirstOrDefault();
                    if (item != null)
                    {
                        wantedItem.SelectedValue = item;
                    }
                }
                if (questStep.RewardItem != null)
                {
                    Item item = items.Where(x => x.ID == questStep.RewardItem).FirstOrDefault();
                    if (item != null)
                    {
                        rewardItem.SelectedValue = item;
                    }
                }
            }
        }

        private void SaveQuestStep(object sender, RoutedEventArgs e)
        {
            currentQuestStep.Description = tbDescription.Text;
            currentQuestStep.RepReqRejectionPhrase = tbRepRejectionPhrase.Text;
            currentQuestStep.StartPhrase = tbStartPhase.Text;
            currentQuestStep.RewardPhrase = tbRewardPhrase.Text;
            currentQuestStep.RejectionPhrase = tbRejectionPhrase.Text;

            if(tbRepReq.Text != "")
                currentQuestStep.RepRequirement = Int32.Parse(tbRepReq.Text);

            if (tbRewardGold.Text != "")
                currentQuestStep.RewardGold = Int32.Parse(tbRewardGold.Text);

            if (tbRepReward.Text != "")
                currentQuestStep.RepReward = Int32.Parse(tbRepReward.Text);

            if (tbRewardXP.Text != "")
                currentQuestStep.RewardXP = Int32.Parse(tbRewardXP.Text);

            Item item = wantedItem.SelectedValue as Item;
            if(item != null)
                currentQuestStep.WantedItem = item.ID;

            item = rewardItem.SelectedValue as Item;
            if(item != null)
                currentQuestStep.RewardItem = item.ID;

            if (npcConfig != null)
                npcConfig.UpdateQuestStep(currentQuestStep);

            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
