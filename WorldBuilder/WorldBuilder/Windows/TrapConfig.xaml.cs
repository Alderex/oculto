﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class TrapConfig : Window
    {
        private Trap currentTrap;

        public TrapConfig(Trap trap)
        {
            InitializeComponent();

            cbSavingThrowType.Items.Add("Intelligence");
            cbSavingThrowType.Items.Add("Dexterity");
            cbSavingThrowType.Items.Add("Strength");

            currentTrap = trap;

            if (trap != null)
            {
                if (trap.Name != null)
                    tbName.Text = trap.Name;
                if (trap.SavingThrow != null)
                    cbSavingThrowType.SelectedValue = trap.SavingThrow;

                tbSavingThrowReq.Text = trap.SavingThrowRequirement.ToString();
                cbAdvanceOnFailure.IsChecked = trap.AdvanceOnFailure;

                if (trap.OnSuccessMessage != null)
                    tbSuccessMessage.Text = trap.OnSuccessMessage;
                if (trap.OnFailureMessage != null)
                    tbFailureMessage.Text = trap.OnFailureMessage;

                tbMinDamage.Text = trap.MinDamage.ToString();
                tbMaxDamage.Text = trap.MaxDamage.ToString();

                if (trap.FailureDestinationID != null)
                    tbFailureDestinationID.Text = trap.FailureDestinationID;
            }
            else
            {
                currentTrap = new Trap();
                currentTrap.ID = Guid.NewGuid().ToString();

                cbSavingThrowType.SelectedIndex = 0;
            }
        }

        private async void SaveTrap(object sender, RoutedEventArgs e)
        {
            currentTrap.Name = tbName.Text;
            currentTrap.SavingThrow = cbSavingThrowType.SelectedValue.ToString();
            currentTrap.SavingThrowRequirement = Int32.Parse(tbSavingThrowReq.Text);
            currentTrap.AdvanceOnFailure = cbAdvanceOnFailure.IsChecked ?? false;
            currentTrap.OnSuccessMessage = tbSuccessMessage.Text;
            currentTrap.OnFailureMessage = tbFailureMessage.Text;
            currentTrap.MinDamage = Int32.Parse(tbMinDamage.Text);
            currentTrap.MaxDamage = Int32.Parse(tbMaxDamage.Text);
            currentTrap.FailureDestinationID = tbFailureDestinationID.Text;

            await currentTrap.Save();
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
