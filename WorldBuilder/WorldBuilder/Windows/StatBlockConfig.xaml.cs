﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Shared.Models.Definitions;
using Shared.Models.Utils;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for NpcConfig.xaml
    /// </summary>
    public partial class StatBlockConfig : Window
    {
        private StatBlock currentStatBlock;

        public StatBlockConfig(StatBlock statBlock)
        {
            InitializeComponent();

            _ = LoadData(statBlock);
        }

        private async Task LoadData(StatBlock statBlock)
        {
            currentStatBlock = statBlock;

            List<Faction> factions = await Faction.GetAll();
            cbFaction.ItemsSource = factions;

            if (currentStatBlock != null)
            {
                if (currentStatBlock.Name != null)
                    name.Text = currentStatBlock.Name;
                tbHitPoints.Text = currentStatBlock.HitPoints.ToString();
                tbMana.Text = currentStatBlock.Mana.ToString();
                tbAC.Text = currentStatBlock.AC.ToString();
                tbMinAttack.Text = currentStatBlock.MinAttack.ToString();
                tbMaxAttack.Text = currentStatBlock.MaxAttack.ToString();
                tbAttackRollBonus.Text = currentStatBlock.AttackRollBonus.ToString();
                tbFactionDeathPenalty.Text = currentStatBlock.FactionDeathPenalty.ToString();
                tbDeathXP.Text = currentStatBlock.DeathXP.ToString();

                if (currentStatBlock.FactionID != null)
                {
                    Faction currentFaction = await Faction.Get(currentStatBlock.FactionID);
                    if (currentFaction != null)
                        cbFaction.SelectedItem = currentFaction;
                }
            }
            else
            {
                currentStatBlock = new StatBlock();
                currentStatBlock.ID = Guid.NewGuid().ToString();
            }
        }

        private async void SaveStatBlock(object sender, RoutedEventArgs e)
        {
            if (name.Text != "")
                currentStatBlock.Name = name.Text;
            if (tbHitPoints.Text != "")
                currentStatBlock.HitPoints = Int32.Parse(tbHitPoints.Text);
            if (tbMana.Text != "")
                currentStatBlock.Mana = Int32.Parse(tbMana.Text);
            if (tbAC.Text != "")
                currentStatBlock.AC = Int32.Parse(tbAC.Text);
            if (tbMinAttack.Text != "")
                currentStatBlock.MinAttack = Int32.Parse(tbMinAttack.Text);
            if (tbMaxAttack.Text != "")
                currentStatBlock.MaxAttack = Int32.Parse(tbMaxAttack.Text);
            if (tbAttackRollBonus.Text != "")
                currentStatBlock.AttackRollBonus = Int32.Parse(tbAttackRollBonus.Text);
            if (tbFactionDeathPenalty.Text != "")
                currentStatBlock.FactionDeathPenalty = Int32.Parse(tbFactionDeathPenalty.Text);
            if (tbDeathXP.Text != "")
                currentStatBlock.DeathXP = Int32.Parse(tbDeathXP.Text);

            if (cbFaction.SelectedIndex == -1)
            {
                currentStatBlock.FactionID = null;
            }
            else
            {
                Faction faction = cbFaction.SelectedValue as Faction;
                currentStatBlock.FactionID = faction.ID;
            }

            await currentStatBlock.Save();
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ClearFaction(object sender, RoutedEventArgs e)
        {
            cbFaction.SelectedIndex = -1;
        }
    }
}
