﻿using System;
using System.Windows;
using System.Windows.Input;
using Shared.Models.Definitions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace WorldBuilder.Windows
{
    /// <summary>
    /// Interaction logic for ItemConfig.xaml
    /// </summary>
    public partial class ItemConfig : Window
    {
        private Item currentItem;

        public ItemConfig(Item item)
        {
            InitializeComponent();

            ddlEquipLocation.Items.Add("Weapon");
            ddlEquipLocation.Items.Add("Offhand");
            ddlEquipLocation.Items.Add("Head");
            ddlEquipLocation.Items.Add("Chest");
            ddlEquipLocation.Items.Add("Hands");
            ddlEquipLocation.Items.Add("Legs");
            ddlEquipLocation.Items.Add("Feet");
            ddlEquipLocation.Items.Add("Neck");
            ddlEquipLocation.Items.Add("Ring");
            ddlEquipLocation.Items.Add("Back");

            onUse.Items.Add("None");
            onUse.Items.Add("Heal");
            onUse.Items.Add("RestoreMana");
            onUse.Items.Add("LearnSpell");
            onUse.Items.Add("Rest");
            onUse.Items.Add("Scroll");
            onUse.Items.Add("Teleport");

            _ = LoadData(item);
        }

        private async Task LoadData(Item item)
        {
            List<Spell> spells = await Spell.GetAll();
            ddlSpells.ItemsSource = spells;

            currentItem = item;

            if (item != null)
            {
                if (item.Name != null)
                    name.Text = item.Name;
                if (item.Description != null)
                    tbDescription.Text = item.Description;
                tbStrengthReq.Text = item.StrengthReq.ToString();
                tbDexterityReq.Text = item.DexterityReq.ToString();
                tbIntelligenceReq.Text = item.IntelligenceReq.ToString();
                tbStrengthBonus.Text = item.StrengthBonus.ToString();
                tbDexterityBonus.Text = item.DexterityBonus.ToString();
                tbIntelligenceBonus.Text = item.IntelligenceBonus.ToString();
                tbWisdomBonus.Text = item.WisdomBonus.ToString();
                tbCharismaBonus.Text = item.CharismaBonus.ToString();
                tbConstitutionBonus.Text = item.ConstitutionBonus.ToString();
                cbUnidentified.IsChecked = item.Unidentified;
                tbMinDamage.Text = item.DamageMin.ToString();
                tbMaxDamage.Text = item.DamageMax.ToString();
                tbAttackRollBonus.Text = item.AttackRollBonus.ToString();
                armor.Text = item.Armor.ToString();
                if (item.OnUseEffect != null)
                    onUse.SelectedValue = item.OnUseEffect;
                if (item.OnUseParameter != null)
                    tbOnUseParam.Text = item.OnUseParameter;
                if (item.OnUseOutput != null)
                    tbOnUseOutput.Text = item.OnUseOutput;
                cbConsumeOnUse.IsChecked = item.ConsumeOnUse;
                price.Text = item.Price.ToString();
                ddlEquipLocation.SelectedIndex = item.EquipLocation;
                perception.Text = item.PerceptionRequired.ToString();
                tbWeight.Text = item.Weight.ToString();

                if (item.ImbuedSpellID != null)
                {
                    Spell spell = await Spell.Get(item.ImbuedSpellID);
                    if (spell != null)
                    {
                        ddlSpells.SelectedItem = spell;
                    }
                }
            }
            else
            {
                currentItem = new Item();
                currentItem.ID = Guid.NewGuid().ToString();
            }
        }

        private void ClearEquipLocation(object sender, RoutedEventArgs e)
        {
            ddlEquipLocation.SelectedIndex = -1;
        }

        private void ClearImbuedSpell(object sender, RoutedEventArgs e)
        {
            ddlSpells.SelectedIndex = -1;
        }

        private async void SaveItem(object sender, RoutedEventArgs e)
        {
            currentItem.Name = name.Text;
            currentItem.Description = tbDescription.Text;
            if (tbStrengthReq.Text != "")
                currentItem.StrengthReq = Int32.Parse(tbStrengthReq.Text);
            if (tbDexterityReq.Text != "")
                currentItem.DexterityReq = Int32.Parse(tbDexterityReq.Text);
            if (tbIntelligenceReq.Text != "")
                currentItem.IntelligenceReq = Int32.Parse(tbIntelligenceReq.Text);
            if (tbStrengthBonus.Text != "")
                currentItem.StrengthBonus = Int32.Parse(tbStrengthBonus.Text);
            if (tbDexterityBonus.Text != "")
                currentItem.DexterityBonus = Int32.Parse(tbDexterityBonus.Text);
            if (tbIntelligenceBonus.Text != "")
                currentItem.IntelligenceBonus = Int32.Parse(tbIntelligenceBonus.Text);
            if (tbWisdomBonus.Text != "")
                currentItem.WisdomBonus = Int32.Parse(tbWisdomBonus.Text);
            if (tbCharismaBonus.Text != "")
                currentItem.CharismaBonus = Int32.Parse(tbCharismaBonus.Text);
            if (tbConstitutionBonus.Text != "")
                currentItem.ConstitutionBonus = Int32.Parse(tbConstitutionBonus.Text);
            currentItem.Unidentified = cbUnidentified.IsChecked ?? false;
            if (tbMinDamage.Text != "")
                currentItem.DamageMin = Int32.Parse(tbMinDamage.Text);
            if (tbMaxDamage.Text != "")
                currentItem.DamageMax = Int32.Parse(tbMaxDamage.Text);
            if (tbAttackRollBonus.Text != "")
                currentItem.AttackRollBonus = Int32.Parse(tbAttackRollBonus.Text);
            if (armor.Text != "")
                currentItem.Armor = Int32.Parse(armor.Text);
            currentItem.OnUseEffect = onUse.Text;
            currentItem.OnUseParameter = tbOnUseParam.Text;
            currentItem.OnUseOutput = tbOnUseOutput.Text;
            currentItem.ConsumeOnUse = cbConsumeOnUse.IsChecked ?? false;
            if (price.Text != "")
                currentItem.Price = Int32.Parse(price.Text);
            if (ddlEquipLocation.SelectedIndex != -1)
                currentItem.EquipLocation = ddlEquipLocation.SelectedIndex;
            else
                currentItem.EquipLocation = -1;
            if (perception.Text != "")
                currentItem.PerceptionRequired = Int32.Parse(perception.Text);
            if (tbWeight.Text != "")
                currentItem.Weight = Int32.Parse(tbWeight.Text);

            if (ddlSpells.SelectedIndex == -1)
            {
                currentItem.ImbuedSpellID = null;
            }
            else
            {
                Spell imbuedSpell = ddlSpells.SelectedItem as Spell;
                currentItem.ImbuedSpellID = imbuedSpell.ID;
            }

            await Item.Assert(currentItem);

            Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^-?0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
