using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OcultoBot.Utils;
using Shared;
using Shared.Models.Definitions;

namespace OcultoBot.Models
{
    public class InstancedRoom : Room
    {
        // The Room this instance was created from
        public string SourceID { get; set; }

        public InstancedRoom()
        { }

        public InstancedRoom(Room room)
        {
            ID = room.ID;
            Name = room.Name;
            Description = room.Description;
            Area = room.Area;
        }

        #region Database Helpers

        public new static async Task<InstancedRoom> Get(string ID)
        {
            return await Database.Instance.Connection.Table<InstancedRoom>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(InstancedRoom instancedRoom)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(instancedRoom);
        }

        public static async Task<int> Delete(InstancedRoom instancedRoom)
        {
            return await Database.Instance.Connection.DeleteAsync(instancedRoom);
        }

        public new static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<InstancedRoom>();
        }

        public new async Task Save()
        {
            await Assert(this);
        }

        #endregion

        public async Task<string> GetSearchResults(InstancedPlayer player)
        {
            int roll = player.Roll();
            roll += player.WisdomBonus;
            if (roll < 1)
                roll = 1;

            //Check for our latest search of this room
            SearchRecord latestRecord = await SearchRecord.GetLatestSearchRecordForRoom(player.ID, ID);
            TimeSpan timeSpan = DateTime.Now - DateTime.Now;

            if (latestRecord != null)
            {
                //Check for our best roll so far
                SearchRecord bestRecord = await SearchRecord.GetBestSearchRecordForRoom(player.ID, ID);

                timeSpan = DateTime.Now - latestRecord.Timestamp;
                
                //Use a time of 5 minutes between searches
                if(timeSpan.Minutes >= 5)
                {
                    if (bestRecord.Roll > roll)
                    {
                        roll = bestRecord.Roll;
                    }
                        
                    await SearchRecord.SaveSearch(player.ID, ID, roll);
                }
                else
                {
                    roll = bestRecord.Roll;
                }
            }
            else
            {
                //Make a record of this search
                await SearchRecord.SaveSearch(player.ID, ID, roll);
            }

            string output = "You search the " + Name + ".";
            output += "\nYou roll a " + roll.ToString() + "!";
            if (timeSpan.Minutes < 5 && (timeSpan.TotalSeconds != 0))
                output += "\n(" + (5 - timeSpan.Minutes) + " minutes before you can get a new search roll)";

            List<Door> doors = await Door.GetDoorsInRoom(this);
            output += "\n\n===== Doors =====";
            foreach (Door door in doors)
            {
                if (roll >= door.PerceptionRequired)
                    output += "\n" + "**" + door.Name + "** - "+ door.Description;
            }

            List<InstancedItem> items = await InstancedItem.GetItemsInRoom(this);
            if (items.Count > 0)
            {
                //Need to hide the header if they don't find anything due to low perception checks
                bool headerAdded = false;
                    
                foreach (Item item in items)
                {
                    if (roll > item.PerceptionRequired)
                    {
                        if(!headerAdded)
                        {
                            headerAdded = true;
                            output += "\n\n===== Items =====";
                        }
                        output += "\n" + "**" + item.Name + "** - " + item.Description;
                    }
                }
            }

            List<InstancedNPC> npcs = await InstancedNPC.GetNPCsForRoom(this);
            if (npcs.Count > 0)
            {
                output += "\n\n===== NPCs =====";
                foreach (InstancedNPC npc in npcs)
                {
                    output += "\n" + "**" + npc.Name + "** - " + npc.Description;
                }
            }

            //Query the database for any players that might be here
            List<InstancedPlayer> players = await InstancedPlayer.GetPlayersInRoom(this);
            InstancedPlayer currentPlayer = players.Where(p => p.ID == player.ID).FirstOrDefault();
            players.Remove(currentPlayer);

            if(players.Count > 0)
            {
                output += "\n\n===== Players =====";
                foreach (InstancedPlayer p in players)
                {
                    output += "\n" + "**" + p.Name + "**";
                    if (p.Resting)
                        output += ". They are currently resting.";
                }
            }

            return output;
        }

        public override string ToString()
        {
            return Name + "\n" + Description;
        }
    }
}
