﻿using Shared;
using Shared.Models.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OcultoBot.Models
{
    public class InstancedDoor : Door
    {
        // The original door this was created from
        public string SourceID { get; set; }

        public InstancedDoor() { }

        public InstancedDoor(Door door)
        {
            ID = door.ID;
            RoomID = door.RoomID;
            Name = door.Name;
            Description = door.Description;
            OnUseOutput = door.OnUseOutput;
            RoomDestination = door.RoomDestination;
            PerceptionRequired = door.PerceptionRequired;
            RequiredKey = door.RequiredKey;
            TrapID = door.TrapID;
        }

        #region Database Helpers

        public new static async Task<InstancedDoor> Get(string ID)
        {
            return await Database.Instance.Connection.Table<InstancedDoor>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<InstancedDoor> GetDoorInRoomByName(InstancedRoom room, string doorName)
        {
            return await Database.Instance.Connection.Table<InstancedDoor>().Where(x => x.RoomID == room.ID && x.Name == doorName).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(InstancedDoor instancedDoor)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(instancedDoor);
        }

        public static async Task<int> Delete(InstancedDoor instancedDoor)
        {
            return await Database.Instance.Connection.DeleteAsync(instancedDoor);
        }

        public new static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<InstancedDoor>();
        }

        public new async Task Save()
        {
            await Assert(this);
        }

        #endregion
    }
}
