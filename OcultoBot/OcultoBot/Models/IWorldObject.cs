using System.Threading.Tasks;

namespace OcultoBot.Models
{
    public interface IWorldObject
    {
        string ID { get; set; }
        string Name { get; set; }
        int HitPoints { get; set; }
        int Mana { get; set; }

        Task TakeDamage(int amount);
        Task<int> Heal(int amount);
        Task Save();
    }
}
