using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Shared.Models.Definitions;
using Shared;
using Shared.Models.Utils;

namespace OcultoBot.Models
{
    public class InstancedNPC : NPC, IWorldObject
    {
        // The NPC this instance was created from
        public string SourceID { get; set; }
        public string RoomID { get; set; }
        public string EnemyID { get; set; }

        public int HitPoints { get; set; }
        public int MaxHitPoints { get; set; }
        public int Mana { get; set; }
        public int MaxMana { get; set; }

        public bool Alive { get { return HitPoints > 0; } }

        public InstancedNPC()
        {
        }

        public InstancedNPC(NPC npc)
        {
            ID = Guid.NewGuid().ToString();
            SourceID = npc.ID;
            WaresCollectionID = npc.WaresCollectionID;
            LootTableID = npc.LootTableID;
            Name = npc.Name;
            Description = npc.Description;
            StatBlockID = npc.StatBlockID;
            IntroPhrase = npc.IntroPhrase;
            WaresRepRequirement = npc.WaresRepRequirement;
            WaresRejectionPhrase = npc.WaresRejectionPhrase;
            RespawnTimer = npc.RespawnTimer;
        }

        public async Task LoadStartingStats()
        {
            StatBlock statBlock = await StatBlock.Get(StatBlockID);
            if (statBlock != null)
            {
                HitPoints = statBlock.HitPoints;
                MaxHitPoints = statBlock.HitPoints;
                Mana = statBlock.Mana;
                MaxMana = statBlock.Mana;
            }
        }

        #region Database Helpers

        public new static async Task<InstancedNPC> Get(string ID)
        {
            return await Database.Instance.Connection.Table<InstancedNPC>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(InstancedNPC npc)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(npc);
        }

        public static async Task<int> Delete(InstancedNPC npc)
        {
            return await Database.Instance.Connection.DeleteAsync(npc);
        }

        public new static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<InstancedNPC>();
        }

        public static async Task<InstancedNPC> GetNpcInRoomByName(InstancedRoom room, string npcName)
        {
            // Order by ID so we get consistent results for rooms with more than one NPC with the same name
            return await Database.Instance.Connection.Table<InstancedNPC>().OrderBy(x => x.ID).Where(n => n.RoomID == room.ID && n.Name.ToLower() == npcName && n.HitPoints > 0).FirstOrDefaultAsync();
        }

        public static async Task<List<InstancedNPC>> GetNPCsForRoom(InstancedRoom room)
        {
            return await Database.Instance.Connection.Table<InstancedNPC>().Where(n => n.RoomID == room.ID && n.HitPoints > 0).ToListAsync();
        }

        private new async Task Save()
        {
            await Assert(this);
        }

        #endregion

        public async Task<string> GetExpandedDescription()
        {
            var statBlock = await GetStatblock();
            string desc = Description;
            if (HitPoints <= statBlock.HitPoints / 2)
                desc += " They appear worn and bloody.";
            return desc;
        }

        public async Task<Faction> GetFaction()
        {
            Faction faction = null;
            StatBlock statBlock = await GetStatblock();
            if (statBlock.FactionID != null)
                faction = await Faction.Get(statBlock.FactionID);

            return faction;
        }

        public async Task<int> GetDamage()
        {
            Random rand = new Random();
            var statBlock = await GetStatblock();
            int amount = rand.Next(statBlock.MinAttack, statBlock.MaxAttack);
            return amount;
        }

        public async Task TakeDamage(int amount)
        {
            HitPoints -= amount;
            if (HitPoints <= 0)
            {
                HitPoints = 0;
                if (RespawnTimer > 0)
                    _ = StartRespawnTimer();
            }

            await Save();
        }

        private async Task StartRespawnTimer()
        {
            await Task.Delay(RespawnTimer * 1000 * 60);

            // Reset our stats
            await LoadStartingStats();
            await Save();
        }

        public async Task<int> Heal(int amount)
        {
            int healAmount = amount;
            HitPoints += amount;
            if (HitPoints > MaxHitPoints)
            {
                healAmount -= HitPoints - MaxHitPoints;
                HitPoints = MaxHitPoints;
            }
            await Save();
            return healAmount;
        }

        public async Task SetEnemy(InstancedPlayer player)
        {
            if (player != null)
                EnemyID = player.ID;
            else
                EnemyID = "";

            await Save();
        }

        public async Task<int> Roll()
        {
            StatBlock statBlock = await GetStatblock();
            Random rand = new Random();
            return rand.Next(0, 21) + statBlock.AttackRollBonus;
        }

        public async Task<Item> GetWaresByName(string name)
        {
            List<Item> waresList = await GetWares();
            foreach (Item item in waresList)
            {
                if (item.Name.ToUpper() == name.ToUpper())
                    return item;
            }
            return null;
        }

        public async Task<List<Item>> GetWares()
        {
            if (WaresCollectionID != null)
            {
                WaresCollection waresCollection = await WaresCollection.Get(WaresCollectionID);
                return await waresCollection.GetItemsInCollection();
            }
            else
            {
                return new List<Item>();
            }
        }

        public async Task<string> GetWaresListing(InstancedPlayer buyer)
        {
            string output = "";
            List<Item> waresList = await GetWares();

            if (waresList.Count > 0)
            {
                output += "===== For Sale =====";
                foreach (Item item in waresList)
                {
                    output += "\n";
                    output += "```";
                    output += await item.WaresOutput(buyer.CharismaDiscount);
                    output += "```";
                }
            }
            return output;
        }
    }
}
