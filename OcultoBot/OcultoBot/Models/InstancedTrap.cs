﻿using Shared;
using Shared.Models.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OcultoBot.Models
{
    public class InstancedTrap : Trap
    {
        // The original trap this was created from
        public string SourceID { get; set; }

        public InstancedTrap() { }

        public InstancedTrap(Trap trap)
        {
            ID = trap.ID;
            Name = trap.Name;
            SavingThrow = trap.SavingThrow;
            SavingThrowRequirement = trap.SavingThrowRequirement;
            AdvanceOnFailure = trap.AdvanceOnFailure;
            OnSuccessMessage = trap.OnSuccessMessage;
            OnFailureMessage = trap.OnFailureMessage;
            MinDamage = trap.MinDamage;
            MaxDamage = trap.MaxDamage;
            FailureDestinationID = trap.FailureDestinationID;
    }

        #region Database Helpers

        public new static async Task<InstancedTrap> Get(string ID)
        {
            return await Database.Instance.Connection.Table<InstancedTrap>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(InstancedTrap instancedTrap)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(instancedTrap);
        }

        public static async Task<int> Delete(InstancedTrap instancedTrap)
        {
            return await Database.Instance.Connection.DeleteAsync(instancedTrap);
        }

        public new static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<InstancedTrap>();
        }

        public new async Task Save()
        {
            await Assert(this);
        }

        #endregion
    }
}
