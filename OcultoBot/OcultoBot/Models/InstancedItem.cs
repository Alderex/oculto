﻿using Shared;
using Shared.Models.Definitions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OcultoBot.Models
{
    public class InstancedItem : Shared.Models.Definitions.Item
    {
        // The Item this instance was created from
        public string SourceID { get; set; }
        public string RoomID { get; set; }

        // Assign a player ID when it's in their inventory
        public string PlayerID { get; set; }
        public bool Equipped { get; set; }

        public InstancedItem() { }

        public InstancedItem(Shared.Models.Definitions.Item item)
        {
            ID = Guid.NewGuid().ToString();
            SourceID = item.ID;
            StrengthReq = item.StrengthReq;
            DexterityReq = item.DexterityReq;
            IntelligenceReq = item.IntelligenceReq;
            StrengthBonus = item.StrengthBonus;
            DexterityBonus = item.DexterityBonus;
            IntelligenceBonus = item.IntelligenceBonus;
            WisdomBonus = item.WisdomBonus;
            CharismaBonus = item.CharismaBonus;
            ConstitutionBonus = item.ConstitutionBonus;
            Unidentified = item.Unidentified;
            Armor = item.Armor;
            ConsumeOnUse = item.ConsumeOnUse;
            DamageMin = item.DamageMin;
            DamageMax = item.DamageMax;
            AttackRollBonus = item.AttackRollBonus;
            EquipLocation = item.EquipLocation;
            OnUseEffect = item.OnUseEffect;
            OnUseOutput = item.OnUseOutput;
            OnUseParameter = item.OnUseParameter;
            PerceptionRequired = item.PerceptionRequired;
            Price = item.Price;
            Weight = item.Weight;
            ImbuedSpellID = item.ImbuedSpellID;

            if (Unidentified)
            {
                if (DamageMax > 0)
                {
                    Name = "Unidentified Weapon";
                    Description = "A weapon with unknown properties.";
                }
                else if (Armor > 0)
                {
                    Name = "Unidentified Armor";
                    Description = "Armor with unknown properties.";
                }
                else
                {
                    Name = "Unidentified Item";
                    Description = "An item with unknown properties.";
                }
            }
            else
            {
                Name = item.Name;
                Description = item.Description;
            }
        }

        #region Database Helpers

        public new static async Task<InstancedItem> Get(string ID)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<InstancedItem>> GetAllUnownedItems()
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.PlayerID == null).ToListAsync();
        }

        public static async Task<int> Assert(InstancedItem instancedItem)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(instancedItem);
        }

        public static async Task<int> Delete(InstancedItem instancedItem)
        {
            return await Database.Instance.Connection.DeleteAsync(instancedItem);
        }

        public static async Task DeleteAllPlayerItems(string playerID)
        {
            await Database.Instance.Connection.Table<InstancedItem>().DeleteAsync(i => i.PlayerID == playerID);
        }

        public new static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<InstancedItem>();
        }

        // Non-player specific
        public static async Task<InstancedItem> GetItemBySourceID(string sourceID)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.SourceID == sourceID).FirstOrDefaultAsync();
        }

        // Player helper methods
        public static async Task<InstancedItem> GetPlayerItemBySourceID(string sourceID, string playerID)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.SourceID == sourceID && i.PlayerID == playerID).FirstOrDefaultAsync();
        }

        public static async Task<InstancedItem> GetPlayerItemByName(string itemName, InstancedPlayer player, bool equipped = false)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.Name.ToUpper() == itemName.ToUpper() && i.PlayerID == player.ID && i.Equipped == equipped).FirstOrDefaultAsync();
        }

        public static async Task<List<InstancedItem>> GetPlayerItems(string playerID, bool equipped = false)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.PlayerID == playerID && i.Equipped == equipped).OrderBy(x => x.Name).ToListAsync();
        }

        public static async Task<InstancedItem> GetPlayerEquipmentBySlot(string playerID, int equipLocation)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.PlayerID == playerID && i.Equipped && i.EquipLocation == equipLocation).FirstOrDefaultAsync();
        }

        public static async Task<List<InstancedItem>> GetPlayerImbuedEquipment(string playerID)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.PlayerID == playerID && i.Equipped && i.ImbuedSpellID != null).ToListAsync();
        }

        // Room helper methods
        public static async Task<List<InstancedItem>> GetItemsInRoom(InstancedRoom room)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.RoomID == room.ID && i.PlayerID == null).ToListAsync();
        }

        public static async Task<InstancedItem> GetItemInRoomByName(InstancedRoom room, string itemName)
        {
            return await Database.Instance.Connection.Table<InstancedItem>().Where(i => i.RoomID == room.ID && i.Name.ToUpper() == itemName.ToUpper() && i.PlayerID == null).FirstOrDefaultAsync();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        #endregion

        public async Task<bool> CanUse()
        {
            bool canUse = true;
            if (PlayerID != null)
            {
                InstancedPlayer owner = await InstancedPlayer.Get(PlayerID);
                if(owner != null)
                {
                    if (owner.Strength < StrengthReq)
                        canUse = false;
                    if (owner.Dexterity < DexterityReq)
                        canUse = false;
                    if (owner.Intelligence < IntelligenceReq)
                        canUse = false;
                }
                else
                {
                    canUse = false;
                }
            }
            else
            {
                canUse = false;
            }

            return canUse;
        }

        public async Task<string> GetDetails()
        {
            string output = "";
            output += "--- " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Name) + " ---";
            output += "\n" + Description;

            if (DamageMax != 0)
                output += "\nDamage: " + DamageMin + "-" + DamageMax;
            if (Armor != 0)
                output += "\nArmor: " + Armor;
            if (StrengthReq != 0)
                output += "\nRequired Strength: " + StrengthReq;
            if (DexterityReq != 0)
                output += "\nRequired Dexterity: " + DexterityReq;
            if (IntelligenceReq != 0)
                output += "\nRequired Intelligence: " + IntelligenceReq;
            if (StrengthBonus != 0)
                output += "\nStrength Bonus: " + StrengthBonus;
            if (DexterityBonus != 0)
                output += "\nDexterity Bonus: " + DexterityBonus;
            if (IntelligenceBonus != 0)
                output += "\nIntelligence Bonus: " + IntelligenceBonus;
            if (WisdomBonus != 0)
                output += "\nWisdom Bonus: " + WisdomBonus;
            if (CharismaBonus != 0)
                output += "\nCharisma Bonus: " + CharismaBonus;
            if (ConstitutionBonus != 0)
                output += "\nConstitution Bonus: " + ConstitutionBonus;

            // Check if this item has an imbued spell
            if (ImbuedSpellID != null)
            {
                Spell spell = await Spell.Get(ImbuedSpellID);
                if (spell != null)
                    output += "\nImbued Spell: " + spell.Name + " (" + spell.Description + ")";
            }

            if (Price != 0)
                output += "\nValue: " + (Price / 2) + " gold";

            return output;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
