﻿using OcultoBot.Utils;
using Shared;
using Shared.Models.Definitions;
using Shared.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OcultoBot.Models
{

    public class InstancedPlayer : Player, IWorldObject
    {
        // The Player this instance was created from
        public string SourceID { get; set; }
        public string RoomID { get; set; }

        public int HitPoints { get; set; }
        public int Mana { get; set; }

        public int CurrentXP { get; set; }
        public int CurrentLevel { get; set; }

        public int AttributePoints { get; set; }

        public int StrengthBonus { get { return Strength - 10; } }
        public int ConstitutionBonus { get { return Constitution - 10; } }
        public int CharismaBonus { get { return Charisma - 10; } }
        public int IntelligenceBonus { get { return Intelligence - 10; } }
        public int WisdomBonus { get { return Wisdom - 10; } }
        public int DexterityBonus { get { return Dexterity - 10; } }

        public int StrengthRollBonus { get { return StrengthBonus / 2; } }
        public int DexterityRollBonus { get { return DexterityBonus / 2; } }
        public int IntelligenceRollBonus { get { return IntelligenceBonus / 2; } }

        public int CarryingCapacity { get { return Strength * 10; } }
        public int BaseAC { get { return Dexterity / 2; } }
        public double CharismaDiscount { get { return (100.0 - (CharismaBonus * 2)) / 100.0; } }
        public int DexterityAttackRollBonus { get { return DexterityBonus / 2; } }

        public int MaxHitPoints { get { return 5 + (CurrentLevel * ((ConstitutionBonus * 2) + 5)); } }
        public int MaxMana { get { return (CurrentLevel) * ((IntelligenceBonus * 2) + 4); } }

        public int XPToNextLevel { get { return (CurrentLevel * CurrentLevel * 10) - CurrentXP; } }
        public DateTime AlarmClock { get; set; }
        public bool Resting { get; set; }
        public bool Alive { get { return HitPoints > 0; } }

        public InstancedPlayer() { }

        public InstancedPlayer(string id, string username, Player player)
        {
            SourceID = player.ID;
            ID = id;
            Name = username;

            CurrentLevel = 1;

            Strength = player.Strength;
            Constitution = player.Constitution;
            Charisma = player.Charisma;
            Intelligence = player.Intelligence;
            Wisdom = player.Wisdom;
            Dexterity = player.Dexterity;

            HitPoints = MaxHitPoints;
            Mana = MaxMana;
            Gold = player.Gold;
            CurrentXP = 0;
            RoomID = "f64e99f3-fb9a-4450-b26b-3dd444b98f2a";
        }

        #region Database Helpers

        public new static async Task<InstancedPlayer> Get(string ID)
        {
            return await Database.Instance.Connection.Table<InstancedPlayer>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(InstancedPlayer instancedPlayer)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(instancedPlayer);
        }

        public static async Task<int> Delete(InstancedPlayer instancedPlayer)
        {
            return await Database.Instance.Connection.DeleteAsync(instancedPlayer);
        }

        public new static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<InstancedPlayer>();
        }

        public static async Task<InstancedPlayer> GetPlayerInRoomByName(InstancedRoom room, string playerName)
        {
            return await Database.Instance.Connection.Table<InstancedPlayer>().Where(x => x.RoomID == room.ID && x.Name.ToUpper() == playerName.ToUpper()).FirstOrDefaultAsync();
        }

        public static async Task<List<InstancedPlayer>> GetPlayersInRoom(InstancedRoom room)
        {
            return await Database.Instance.Connection.Table<InstancedPlayer>().Where(p => p.RoomID == room.ID).ToListAsync();
        }

        public async Task Save()
        {
            await Assert(this);
        }

        #endregion

        public int Roll()
        {
            Random rand = new Random();
            return rand.Next(1, 21);
        }

        public async Task<int> AttackRoll()
        {
            Random rand = new Random();
            int roll = rand.Next(1, 21);
            int totalAttackRollBonus = DexterityAttackRollBonus + await GetAttackRollBonusFromEquipment();

            return roll + totalAttackRollBonus;
        }

        private async Task<int> GetAttackRollBonusFromEquipment()
        {
            int bonus = 0;
            List<InstancedItem> equipment = await InstancedItem.GetPlayerItems(ID, true);
            foreach(InstancedItem item in equipment)
            {
                bonus += item.AttackRollBonus;
            }
            return bonus;
        }

        public int GetSavingThrow(string savingThrowType)
        {
            Random rand = new Random();
            int roll = rand.Next(1, 21);
            int rollBonus = 0;

            if (savingThrowType == "Dexterity")
                rollBonus += DexterityRollBonus;
            else if (savingThrowType == "Strength")
                rollBonus += StrengthRollBonus;
            else if (savingThrowType == "Intelligence")
                rollBonus += IntelligenceRollBonus;

            return roll + rollBonus;
        }

        public async Task<string> GetStats()
        {
            string output = "===== Stats for " + Name + " =====";
            output += "\nLevel: **" + CurrentLevel.ToString() + "**";
            output += "\nHit Points: **[" + HitPoints + "/" + MaxHitPoints + "]**";
            output += "\nMana: **[" + Mana + "/" + MaxMana + "]**";
            output += "\nXP Until Level " + (CurrentLevel + 1).ToString() + ": **" + XPToNextLevel.ToString() + "**";
            output += "\n\nStrength: **" + Strength + "**";
            output += "\nConstitution: **" + Constitution + "**";
            output += "\nCharisma: **" + Charisma + "**";
            output += "\nIntelligence: **" + Intelligence + "**";
            output += "\nWisdom: **" + Wisdom + "**";
            output += "\nDexterity: **" + Dexterity + "**";
            output += "\nAvailable Points: **" + AttributePoints + "**";
            int totalAC = await GetAC();
            output += "\n\nAC: **" + totalAC.ToString() + "** (" + BaseAC + " base AC)";

            int equipmentAttackRollBonus = await GetAttackRollBonusFromEquipment();
            if(equipmentAttackRollBonus != 0)
            {
                int totalAttackBonus = DexterityAttackRollBonus + equipmentAttackRollBonus;
                output += "\nAttack Roll Bonus: **" + totalAttackBonus + "** (" + equipmentAttackRollBonus + " from equipment)";
            }
            else
            {
                output += "\nAttack Roll Bonus: **" + DexterityBonus + "**";
            }

            InstancedItem weapon = await InstancedItem.GetPlayerEquipmentBySlot(ID, EquipLocation.WEAPON);
            if (weapon != null)
                output += "\nWeapon Damage: **" + weapon.DamageMin + " - " + weapon.DamageMax + "**";

            return output;
        }

        //Return true if you level up
        public async Task<bool> AddXP(int amount)
        {
            CurrentXP += amount;
            if (CurrentXP >= CurrentLevel * CurrentLevel * 10)
            {
                CurrentXP = 0;
                CurrentLevel++;
                HitPoints = MaxHitPoints;
                Mana = MaxMana;
                AttributePoints += 2;
                await Save();
                return true;
            }
            await Save();
            return false;
        }

        public async Task Die()
        {
            await SearchRecord.DeletePlayerSearchRecords(ID);
            await Database.Instance.Connection.DeleteAsync(this);
        }

        public async Task<string> DropAllItems(InstancedRoom room)
        {
            string output = "";

            List<InstancedItem> inventoryItems = await InstancedItem.GetPlayerItems(ID);
            //List<InstancedItem> equippedItems = await InstancedItem.GetPlayerItems(ID, true);
            //inventoryItems.AddRange(equippedItems);

            foreach (InstancedItem item in inventoryItems)
            {
                item.RoomID = room.ID;
                item.PlayerID = null;
                await item.Save();

                output += "\n" + Name + " drops a **" + item.Name + "**.";
            }

            return output;
        }

        public async Task Rest(int minutes)
        {
            AlarmClock = DateTime.Now.AddMinutes(minutes);
            Resting = true;
            await Save();
        }

        public async Task FinishRest()
        {
            Resting = false;
            HitPoints = MaxHitPoints;
            Mana = MaxMana;
            await Save();
        }

        #region Inventory

        public async Task<int> GetInventoryWeight()
        {
            int weight = 0;

            List<InstancedItem> items = await InstancedItem.GetPlayerItems(ID);
            items.AddRange(await InstancedItem.GetPlayerItems(ID, true));
            foreach (InstancedItem item in items)
            {
                weight += item.Weight;
            }

            return weight;
        }

        public async Task<bool> AddItem(InstancedItem item)
        {
            item.PlayerID = ID;
            int result = await InstancedItem.Assert(item);
            return result == 1;
        }

        public async Task<InstancedItem> GetItemBySourceID(string itemSourceID)
        {
            return await InstancedItem.GetPlayerItemBySourceID(itemSourceID, ID);
        }

        public async Task<bool> DestroyItem(string id)
        {
            InstancedItem item = await InstancedItem.Get(id);
            if (item != null)
            {
                await InstancedItem.Delete(item);
                return true;
            }

            return false;
        }

        public async Task<string> GetInventoryListing()
        {
            string output = "\n===== Inventory =====";
            output += "```";
            output += Gold.ToString() + " gold";

            int totalWeight = 0;

            List<InstancedItem> items = await InstancedItem.GetPlayerItems(ID);
            if (items.Count > 0)
            {
                foreach (InstancedItem item in items)
                {
                    output += "\n" + item.Name;
                    if (item.Weight == 1)
                        output += " - " + item.Weight + " lb";
                    else if (item.Weight > 1)
                        output += " - " + item.Weight + " lbs";
                    totalWeight += item.Weight;
                }
            }
            output += "```";
            List<InstancedItem> equipment = await InstancedItem.GetPlayerItems(ID, true);
            if (equipment.Count > 0)
            {
                output += "\n===== Equipment =====";
                output += "```";
                foreach (InstancedItem item in equipment)
                {
                    output += "\n" + item.Name;
                    if (item.Weight == 1)
                        output += " - " + item.Weight + " lb";
                    else if (item.Weight > 1)
                        output += " - " + item.Weight + " lbs";
                    totalWeight += item.Weight;
                }
                output += "```";
            }

            output += "\nTotal Weight: " + totalWeight.ToString() + " / " + CarryingCapacity + " lbs";

            return output;
        }

        public async Task GiveGold(int amount)
        {
            Gold += amount;
            await Save();
        }

        public async Task<bool> SpendGold(int amount)
        {
            if (Gold >= amount)
            {
                Gold -= amount;
                await Save();
                return true;
            }
            return false;
        }

        #endregion

        #region Equipment

        public async Task<bool> EquipItem(InstancedItem item)
        {
            if (item.EquipLocation >= 0)
            {
                //Do we currently have an item in this items slot?
                InstancedItem currentItem = await InstancedItem.GetPlayerEquipmentBySlot(ID, item.EquipLocation);
                if (currentItem != null)
                {
                    await UnequipItem(currentItem);
                }

                item.Equipped = true;
                await InstancedItem.Assert(item);

                // If this item has bonus attributes, add them to our player until its unequipped
                if (item.StrengthBonus > 0)
                    Strength += item.StrengthBonus;
                if (item.DexterityBonus > 0)
                    Dexterity += item.DexterityBonus;
                if (item.IntelligenceBonus > 0)
                    Intelligence += item.IntelligenceBonus;
                if (item.WisdomBonus > 0)
                    Wisdom += item.WisdomBonus;
                if (item.CharismaBonus > 0)
                    Charisma += item.CharismaBonus;
                if (item.ConstitutionBonus > 0)
                    Constitution += item.ConstitutionBonus;

                await Save();

                return true;
            }

            return false;
        }

        public async Task<bool> UnequipItemByName(string name)
        {
            InstancedItem item = await InstancedItem.GetPlayerItemByName(name, this, true);
            if (item != null)
                return await UnequipItem(item);
            else
                return false;
        }

        public async Task<bool> UnequipItem(InstancedItem item)
        {
            if (item != null)
            {
                item.Equipped = false;
                await InstancedItem.Assert(item);

                // If this item has bonus attributes, remove them from our player
                if (item.StrengthBonus > 0)
                    Strength -= item.StrengthBonus;
                if (item.DexterityBonus > 0)
                    Dexterity -= item.DexterityBonus;
                if (item.IntelligenceBonus > 0)
                    Intelligence -= item.IntelligenceBonus;
                if (item.WisdomBonus > 0)
                    Wisdom -= item.WisdomBonus;
                if (item.CharismaBonus > 0)
                    Charisma -= item.CharismaBonus;
                if (item.ConstitutionBonus > 0)
                    Constitution -= item.ConstitutionBonus;

                await Save();

                return true;
            }

            return false;
        }

        public async Task<string> GetEquipmentReadout()
        {
            List<InstancedItem> playerEquipment = await InstancedItem.GetPlayerItems(ID, true);
            string output = "===== Equipment =====";
            if (playerEquipment.Count > 0)
            {
                foreach (InstancedItem item in playerEquipment)
                {
                    output += "```";
                    output += await item.GetDetails();
                    output += "```";
                }
            }
            else
            {
                output += "\nNothing";
            }                

            return output;
        }

        public async Task<int> GetAC()
        {
            int equipmentAC = await GetEquipmentAC();
            return BaseAC + equipmentAC;
        }

        public async Task<int> GetEquipmentAC()
        {
            int equipmentAC = 0;
            List<InstancedItem> playerEquipment = await InstancedItem.GetPlayerItems(ID, true);
            foreach (InstancedItem item in playerEquipment)
            {
                equipmentAC += item.Armor;
            }
            return equipmentAC;
        }

        #endregion

        #region Spellbook

        public async Task<bool> LearnSpell(string spellName)
        {
            if (spellName != "")
            {
                // Do we already know this spell?
                Spell spell = await Spellbook.GetPlayerSpellByName(spellName, ID);
                if (spell == null)
                {
                    // We don't know this spell so we can try and learn it
                    await Spellbook.AddSpell(ID, spellName);
                    return true;
                }
            }

            return false;
        }

        public async Task<string> GetSpells()
        {
            List<Spell> spells = await Spellbook.GetSpells(ID);
            string output = "===== Spellbook =====";
            if (spells.Count > 0)
            {
                foreach (Spell spell in spells)
                {
                    output += "```";
                    output += "\n~~ " + spell.Name + " ~~";
                    output += "\n" + spell.Description;
                    output += "\nMana Cost: " + spell.ManaCost;
                    output += "```";
                }
            }
            else
            {
                output += "\nNothing";
            }

            List<InstancedItem> imbuedEquipment = await InstancedItem.GetPlayerImbuedEquipment(ID);
            if(imbuedEquipment.Count > 0)
            {
                output = "\n\n===== Imbued Equipment =====";
                foreach (InstancedItem item in imbuedEquipment)
                {
                    Spell spell = await Spell.Get(item.ImbuedSpellID);
                    if (spell != null)
                    {
                        output += "```";
                        output += "\n~~ " + spell.Name + " ~~";
                        output += "\n" + spell.Description;
                        output += "\nMana Cost: " + spell.ManaCost;
                        output += "```";
                    }
                }
            }
                

            return output;
        }




        #endregion

        #region Combat

        public async Task<string> GetWeaponName()
        {
            InstancedItem equippedItem = await InstancedItem.GetPlayerEquipmentBySlot(ID, EquipLocation.WEAPON);
            if (equippedItem != null)
                return equippedItem.Name;
            else
                return "fists";
        }

        public async Task<int> GetDamage()
        {
            InstancedItem equippedItem = await InstancedItem.GetPlayerEquipmentBySlot(ID, EquipLocation.WEAPON);
            Random rand = new Random();
            if (equippedItem != null)
                return rand.Next(equippedItem.DamageMin, equippedItem.DamageMax);
            return rand.Next(1, 3);
        }

        public async Task TakeDamage(int amount)
        {
            HitPoints -= amount;
            if (HitPoints <= 0)
                HitPoints = 0;

            await Save();
        }

        public async Task<int> Heal(int amount)
        {
            int healAmount = amount;
            HitPoints += amount;
            if (HitPoints > MaxHitPoints)
            {
                healAmount -= HitPoints - MaxHitPoints;
                HitPoints = MaxHitPoints;
            }
            await Save();
            return healAmount;
        }

        public async Task<int> RestoreMana(int amount)
        {
            int restoredMana = amount;
            Mana += amount;
            if (Mana > MaxMana)
            {
                restoredMana -= Mana - MaxMana;
                Mana = MaxMana;
            }
            await Save();
            return restoredMana;
        }

        #endregion
    }
}

