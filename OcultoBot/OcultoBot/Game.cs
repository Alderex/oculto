﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using OcultoBot.Models;
using OcultoBot.Utils;
using Shared;
using Shared.Models.Definitions;
using Shared.Models.Utils;
using Shared.Models.WorldStart;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace OcultoBot
{
    class Game
    {
        private CommandService commands;
        private DiscordSocketClient client;
        private Database db;

        public static void Main(string[] args)
        {
            bool worldLoaded = false;
            string dbPath = "";

            while (!worldLoaded)
            {
                Console.WriteLine("Please specify your world:");
                dbPath = Console.ReadLine();
                //dbPath = "World.db3";
                //dbPath = Directory.GetCurrentDirectory() + '\\' + dbPath;
                if (File.Exists(dbPath))
                {
                    Console.WriteLine("World Loaded!");
                    worldLoaded = true;
                }
                else
                {
                    Console.WriteLine("World not found!");
                }
            }

            new Game().Start(dbPath).GetAwaiter().GetResult();
        }

        public async Task Start(string dbPath)
        {
            client = new DiscordSocketClient();
            commands = new CommandService();

            db = new Database(dbPath);
            await db.Init();

            //Initialize the tables that we'll need for our instanced data
            await db.Connection.CreateTableAsync<InstancedItem>();
            await db.Connection.CreateTableAsync<InstancedNPC>();
            await db.Connection.CreateTableAsync<InstancedPlayer>();
            await db.Connection.CreateTableAsync<InstancedRoom>();
            await db.Connection.CreateTableAsync<InstancedDoor>();
            await db.Connection.CreateTableAsync<InstancedTrap>();
            await db.Connection.CreateTableAsync<Spellbook>();
            await db.Connection.CreateTableAsync<SearchRecord>();
            await db.Connection.CreateTableAsync<FactionRep>();

            //Populate our instances with the starting data provided from the WorldBuilder
            List<Room> startingRooms = await Room.GetAll();
            foreach (Room startingRoom in startingRooms)
            {
                InstancedRoom instancedRoom = new InstancedRoom(startingRoom);
                await instancedRoom.Save();
            }

            List<Door> startingDoors = await Door.GetAll();
            foreach (Door startingDoor in startingDoors)
            {
                InstancedDoor instancedDoor = new InstancedDoor(startingDoor);
                await instancedDoor.Save();
            }

            List<Trap> startingTraps = await Trap.GetAll();
            foreach (Trap startingTrap in startingTraps)
            {
                InstancedTrap instancedTrap = new InstancedTrap(startingTrap);
                await instancedTrap.Save();
            }

            await InstancedNPC.DeleteAll();
            List<StartingNPC> startingNPCs = await StartingNPC.GetAll();
            foreach (StartingNPC startingNPC in startingNPCs)
            {
                NPC sourceNPC = await NPC.Get(startingNPC.SourceID);
                InstancedNPC instancedNPC = new InstancedNPC(sourceNPC);
                instancedNPC.RoomID = startingNPC.RoomID;
                await instancedNPC.LoadStartingStats();
                await instancedNPC.Save();
            }

            // Delete all items that have accumulated on the ground before setting out new starting items
            List<InstancedItem> unownedItems = await InstancedItem.GetAllUnownedItems();
            foreach (InstancedItem unownedItem in unownedItems)
                await InstancedItem.Delete(unownedItem);

            List<StartingItem> startingItems = await StartingItem.GetAll();
            foreach (StartingItem startingItem in startingItems)
            {
                Item sourceItem = await Item.Get(startingItem.SourceID);
                InstancedItem instancedItem = new InstancedItem(sourceItem);
                instancedItem.RoomID = startingItem.RoomID;
                await instancedItem.Save();
            }

            client.MessageReceived += Client_MessageReceived;
            client.Log += Log;

            await client.LoginAsync(TokenType.Bot, "MTkwMTk2NTk3NDAzOTQyOTIz.DbeQwA.e4nys1ZayMU7fi2FfUN7HSm05L0");
            await client.StartAsync();

            //Await this task until the program is closed
            await Task.Delay(-1);
        }

        private async Task Client_MessageReceived(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            if (message == null)
                return;

            if (!message.Content.StartsWith(">"))
                return;

            string command = "";
            string parameter = "";

            if (message.Content.Contains(" "))
            {
                command = message.Content.ToLower().Substring(0, message.Content.ToLower().IndexOf(' '));
                parameter = message.Content.ToLower().Substring(command.Length + 1, message.Content.Length - command.Length - 1);
            }
            else
            {
                command = message.Content.ToLower();
            }

            var player = await InstancedPlayer.Get(message.Author.AvatarId);

            if (command == ">help")
                await Help(message);

            else if (command == ">adventure")
                await RegisterPlayer(message, parameter);

            else if (player == null)
            {
                await message.Channel.SendMessageAsync("You must create a character before interacting with me.");
                return;
            }

            // Put this catch here for when they type help or adventure
            if (player == null)
                return;

            if (player.Resting)
            {
                if (DateTime.Now > player.AlarmClock)
                {
                    await player.FinishRest();
                    await message.Channel.SendMessageAsync(player.Name + ", you open your eyes feeling refreshed.");
                }
                else
                {
                    int minutesLeft = (player.AlarmClock - DateTime.Now).Minutes;
                    int secondsLeft = (player.AlarmClock - DateTime.Now).Seconds;

                    if (minutesLeft > 0)
                        await message.Channel.SendMessageAsync(player.Name + ", you are sleeping soundly for another **" + minutesLeft + "** minutes.");
                    else
                        await message.Channel.SendMessageAsync(player.Name + ", you are sleeping soundly for another **" + secondsLeft + "** seconds.");
                    return;
                }
            }

            switch (command)
            {
                case ">see":
                    await See(player, message.Channel);
                    break;
                case ">search":
                    await Search(player, message.Channel);
                    break;
                case ">examine":
                    if (parameter != "")
                        await Examine(player, message.Channel, parameter);
                    break;
                case ">take":
                    if (parameter != "")
                        await Take(player, message.Channel, parameter);
                    break;
                case ">give":
                    if (parameter != "")
                        await Give(player, message.Channel, parameter);
                    break;
                case ">talk":
                    if (parameter != "")
                        await Talk(player, message.Channel, parameter);
                    break;
                case ">use":
                    if (parameter != "")
                        await Use(player, message.Channel, parameter);
                    break;
                case ">spellbook":
                    await Spellbook(player, message.Channel, parameter);
                    break;
                case ">cast":
                    if (parameter != "")
                        await Cast(player, message.Channel, parameter);
                    break;
                case ">buy":
                    if (parameter != "")
                        await Buy(player, message.Channel, parameter);
                    break;
                case ">sell":
                    if (parameter != "")
                        await Sell(player, message.Channel, parameter);
                    break;
                case ">inventory":
                    await Inventory(player, message.Channel);
                    break;
                case ">equip":
                    if (parameter != "")
                        await Equip(player, message.Channel, parameter);
                    break;
                case ">unequip":
                    if (parameter != "")
                        await Unequip(player, message.Channel, parameter);
                    break;
                case ">equipment":
                    await Equipment(player, message.Channel);
                    break;
                case ">stats":
                    await Stats(player, message.Channel);
                    break;
                case ">train":
                    await Train(player, message.Channel, parameter);
                    break;
                case ">drop":
                    if (parameter != "")
                        await Drop(player, message.Channel, parameter);
                    break;
                case ">attack":
                    if (parameter != "")
                        await Attack(player, message.Channel, parameter);
                    break;
                case ">rep":
                    await Rep(player, message.Channel);
                    break;
                case ">suicide":
                    await Suicide(player, message.Channel);
                    break;
            }
        }

        private async Task Help(SocketUserMessage message)
        {
            string output = "W̶̟͈̞̣̲h͇̹̙̪̼͖̬o͏͉̬ͅ d̜͇͉͕̣a̵̝̝r̤̣̹̦̪͘e͏̘͖̺s̱̯̮͞ ̺̺̟̻͔͔̬a̖͚͕̳̬wak̺̯̙ͅe̛̲̜̘̼n̫̥ ̧͍̥̳̯t͈͉̰h̶̗͙͉̩͎̝̟e ҉̙̗̟̻L͇̦̤̲̟̦͞o̷̠̮̠̮͔r̭̝͍ḑ̫̯̯͚͉̳̙ ̴̤̻̪̳̯o̮̙̻̹̖̫̤f͉͚̣͍̗̻ ̶͈̖ͅB̺̣̳͍͈͙ͅọ͚͕n͙͚̲̺͜e̞̤̗̯̦̜̭s͕̝̺̰ͅ?͉̻̤̞͡ͅ";
            output += "\n\nTo get started, type in **>adventure**.";
            output += "\nUse the following commands to play:\n";

            output += "\n**General**\n";
            output += "```";
            output += ">see: What you see around you.\n";
            output += ">search: Search your current area to see what you can interact with.\n";
            output += ">examine: Examine an item in your possession.\n";
            output += ">equipment: See what you're currently equipped with.\n";
            output += ">inventory: See what you're currently holding.\n";
            output += ">spellbook: See what spells you know.\n";
            output += ">cast [spell name] on [target]: Cast a spell.\n";
            output += ">stats: See what your current character stats are.\n";
            output += ">train [attribute]: Improve an attribute (strength, dexterity, intelligence, charisma, wisdom, constitution).\n";
            output += "```";
            output += "\n**Items**\n";
            output += "```";
            output += ">take [item name]: Pick up an item you see in the area.\n";
            output += ">drop [item name]: Drop an item from your inventory to the ground.\n";
            output += ">use [item name]: Try to use something you can see or something that is in your inventory.\n";
            output += ">use [item name] on [target]: Some items can be used on items or other people!\n";
            output += ">buy [item name]: Try to purchase an item that is available in your area.\n";
            output += ">sell [item name]: Sell an item from your inventory. (Examine an item to see its value!).\n";
            output += ">equip [item name]: Equip an item from your inventory.\n";
            output += ">unequip [item name]: Unequip an item from your person.\n";
            output += "```";
            output += "\n**NPCs**\n";
            output += "```";
            output += ">give [item name] to [npc name]: Try to give an item to an NPC.  They may be looking for something in particular!\n";
            output += ">talk [npc name]: Talk to an NPC you can see.\n";
            output += ">attack [npc name]: Attack an NPC or player with your currently equipped weapon.\n";
            output += ">rep: Get your current standing with various NPC factions.";
            output += "```";

            await message.Channel.SendMessageAsync(output);
        }

        private async Task RegisterPlayer(SocketUserMessage message, string parameter)
        {
            var dbPlayer = await InstancedPlayer.Get(message.Author.AvatarId);

            if (dbPlayer == null)
            {
                Player playerTemplate = await Player.GetByName(parameter);

                if (playerTemplate != null)
                {
                    var player = new InstancedPlayer(message.Author.AvatarId, message.Author.Username, playerTemplate);
                    await player.Save();

                    List<StartingInventory> startingInventoryItems = await StartingInventory.GetStartingInventoryForPlayer(playerTemplate.ID);
                    foreach (StartingInventory startingItem in startingInventoryItems)
                    {
                        Item sourceItem = await Item.Get(startingItem.SourceID);
                        InstancedItem item = new InstancedItem(sourceItem);
                        item.PlayerID = player.ID;
                        await item.Save();
                    }

                    string output = "Welcome to the game " + message.Author.Username + ".";
                    output += "\nCenturies ago, a tower was summoned by a powerful wizard at the center of the continent. Its sudden appearance caused a shockwave, destroying everything for miles." +
                        " Rumors say that anyone who enters the tower and climbs their way to the final floor is rewarded with riches beyond their imagination." +
                        " You've decided to investigate this tower yourself and so you make the journey to its ominous black gates. When you arrive, you push through the doors and you are suddenly blinded by white light. " +
                        " Once your eyes adjust, you see that you're in a tavern. Here is where your story begins.";
                    await message.Channel.SendMessageAsync(output);
                }
                else
                {
                    string output = "Greetings " + message.Author.Username + "!";
                    output += "\nIn order to begin, please specify your class. This determines your starting ability scores.";
                    output += "\nFor example, you could type: `>adventure fighter`";
                    output += "\nYou have the following classes to choose from:";

                    List<Player> players = await Player.GetAll();
                    foreach (Player player in players)
                    {
                        output += "```" + player.Name + "```";
                    }

                    await message.Channel.SendMessageAsync(output);
                }
            }
            else
            {
                await message.Channel.SendMessageAsync(message.Author.Username + ", you've already joined.");
            }
        }

        private async Task See(InstancedPlayer player, ISocketMessageChannel channel)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);
            await channel.SendMessageAsync(room.Description);
        }

        private async Task Search(InstancedPlayer player, ISocketMessageChannel channel)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);
            string results = await room.GetSearchResults(player);
            await channel.SendMessageAsync(results);
        }

        private async Task Examine(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            string output = "";

            // See if there is an NPC that we can examine
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);
            InstancedNPC npc = await InstancedNPC.GetNpcInRoomByName(room, target);

            if (npc != null)
            {
                output = await npc.GetExpandedDescription();
            }

            if (output == "")
            {
                InstancedItem item = await InstancedItem.GetPlayerItemByName(target, player);
                if (item == null)
                    item = await InstancedItem.GetPlayerItemByName(target, player, true);
                if (item == null)
                    item = await InstancedItem.GetItemInRoomByName(room, target);

                if (item != null)
                {
                    output += "You take a closer look at the " + target + ".";
                    output += "```";
                    output += await item.GetDetails();
                    output += "```";
                }
                else
                {
                    output = "You don't see a " + target + ".";
                }
            }

            await channel.SendMessageAsync(output);
        }

        private async Task Take(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);
            InstancedItem item = await InstancedItem.GetItemInRoomByName(room, target);

            if (item != null)
            {
                await OpportunityAttacks(player, room, channel);
                if (player != null && player.Alive)
                {
                    // Check if this is going to go over our carry limit
                    int currentWeight = await player.GetInventoryWeight();
                    int itemWeight = item.Weight;
                    if (currentWeight + itemWeight > player.CarryingCapacity)
                    {
                        await channel.SendMessageAsync("You're carrying too much to take the " + target + ".");
                    }
                    else
                    {
                        await player.AddItem(item);
                        await channel.SendMessageAsync("You take the " + target + ".");
                    }
                }
            }
            else
            {
                await channel.SendMessageAsync("You can't retrieve a " + target + ".");
            }
        }

        private async Task Give(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);

            //Make sure the target has two parts - recipient and item
            if (target.Contains(" to "))
            {
                string itemName = target.Substring(0, target.IndexOf(" to "));
                string recipient = target.Substring(itemName.Length + 4, target.Length - itemName.Length - 4);

                InstancedItem item = await InstancedItem.GetPlayerItemByName(itemName, player);
                if (item != null)
                {
                    //Check to see if the recipient matches the name of an NPC in this room
                    InstancedNPC npc = await InstancedNPC.GetNpcInRoomByName(room, recipient);
                    if (npc != null)
                    {
                        await OpportunityAttacks(player, room, channel);

                        if (player != null && player.Alive && npc.EnemyID != player.ID)
                        {
                            QuestStep questStep = await QuestStep.GetCurrentQuestStepForPlayer(npc.SourceID, player.ID);
                            if (questStep != null)
                            {
                                bool npcWillingToAccept = true;

                                // If this quest step has a reputation requirement, ensure that we meet it
                                Faction npcFaction = await npc.GetFaction();
                                if (questStep.RepRequirement > 0 && npcFaction != null)
                                {
                                    FactionRep factionRep = await FactionRep.GetFactionRepForPlayer(npcFaction, player.ID);
                                    if (factionRep.Reputation < questStep.RepRequirement * player.CharismaBonus)
                                        npcWillingToAccept = false;
                                }

                                if (npcWillingToAccept)
                                {
                                    if (questStep.WantedItem != null && item.SourceID == questStep.WantedItem)
                                    {
                                        string output = questStep.RewardPhrase;
                                        await player.DestroyItem(item.ID);

                                        if (questStep.RewardGold != 0)
                                        {
                                            await player.GiveGold(questStep.RewardGold);
                                            output += "\n";
                                            output += "You receive **" + questStep.RewardGold + " gold**.";
                                        }

                                        Item reward = await Item.Get(questStep.RewardItem);
                                        if (reward != null)
                                        {
                                            InstancedItem instancedReward = new InstancedItem(reward);
                                            instancedReward.PlayerID = player.ID;
                                            await instancedReward.Save();

                                            output += "\n";
                                            output += "You receive a **" + instancedReward.Name + "**.";
                                        }

                                        if (questStep.RepReward != 0)
                                        {
                                            if (npcFaction != null)
                                            {
                                                FactionRep factionRep = await FactionRep.GetFactionRepForPlayer(npcFaction, player.ID);
                                                factionRep.Reputation += questStep.RepReward;
                                                await factionRep.Save();

                                                output += "\n";
                                                output += "You gain **" + questStep.RepReward.ToString() + "** reputation with " + npcFaction.Name + "!";
                                            }
                                        }

                                        bool levelUp = await player.AddXP(questStep.RewardXP);
                                        output += "\nYou are awarded **" + questStep.RewardXP + "** XP.";
                                        if (levelUp)
                                            output += "\nYou level up! You are now level **" + player.CurrentLevel + "**!";

                                        // Increment our quest progress, and if there's no next step, wipe our progress to cycle the quest
                                        QuestProgress currentProgress = await QuestProgress.GetPlayerProgressOnQuest(player.ID, npc.SourceID);
                                        QuestStep nextStep = await QuestStep.GetNextQuestStepForPlayer(npc.SourceID, player.ID);
                                        if (nextStep != null)
                                        {
                                            currentProgress.QuestStepID = nextStep.ID;
                                            await currentProgress.Save();
                                        }
                                        else
                                        {
                                            await QuestProgress.Delete(currentProgress);
                                        }

                                        await channel.SendMessageAsync(output);
                                    }
                                    else if (questStep.RejectionPhrase != null)
                                    {
                                        await channel.SendMessageAsync(questStep.RejectionPhrase);
                                    }
                                }
                                else
                                {
                                    await channel.SendMessageAsync(questStep.RepReqRejectionPhrase);
                                }
                            }
                            else
                            {
                                await channel.SendMessageAsync("They don't aceept your " + item.Name);
                            }
                        }
                        else
                        {
                            await channel.SendMessageAsync(npc.Name + " is not on speaking terms with you " + player.Name);
                        }
                    }
                    else
                    {
                        await channel.SendMessageAsync("You don't see an NPC named " + recipient + ".");
                    }
                }
                else
                {
                    await channel.SendMessageAsync("You can't give a " + itemName + " if you don't have one.");
                }
            }
            else
            {
                await channel.SendMessageAsync("I don't understand your request.");
            }
        }

        private async Task Equip(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedItem item = await InstancedItem.GetPlayerItemByName(target, player);
            if (item == null)
            {
                await channel.SendMessageAsync("You don't have anything by that name to equip.");
            }
            else if (!item.Unidentified)
            {
                bool canUse = await item.CanUse();
                if (canUse)
                {
                    bool equipped = await player.EquipItem(item);
                    if (equipped)
                        await channel.SendMessageAsync("You equip your " + target + ".");
                    else
                        await channel.SendMessageAsync("You fail to equip your " + target + ".");
                }
                else
                {
                    await channel.SendMessageAsync("You lack the requirements to equip this item.");
                }
            }
            else
            {
                await channel.SendMessageAsync("You can not equip unidentified items.");
            }
        }

        private async Task Unequip(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            bool unequipped = await player.UnequipItemByName(target);
            if (unequipped)
                await channel.SendMessageAsync("You unequip your " + target + ".");
            else
                await channel.SendMessageAsync("You fail to unequip your " + target + ".");
        }

        private async Task Equipment(InstancedPlayer player, ISocketMessageChannel channel)
        {
            string output = await player.GetEquipmentReadout();
            await channel.SendMessageAsync(output);
        }

        private async Task Stats(InstancedPlayer player, ISocketMessageChannel channel)
        {
            string output = await player.GetStats();
            await channel.SendMessageAsync(output);
        }

        private async Task Train(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            string output = "";
            if (player.AttributePoints > 0)
            {
                switch (target.ToLower())
                {
                    case "strength":
                        player.Strength++;
                        player.AttributePoints--;
                        await player.Save();
                        output = "You spend some time strength training, increasing your strength to **" + player.Strength + "**!";
                        break;
                    case "dexterity":
                        player.Dexterity++;
                        player.AttributePoints--;
                        await player.Save();
                        output = "You spend some time increasing your dexterity, increasing it to **" + player.Dexterity + "**!";
                        break;
                    case "intelligence":
                        player.Intelligence++;
                        player.AttributePoints--;
                        await player.Save();
                        output = "You study the arcane arts, increasing your intelligence to **" + player.Intelligence + "**!";
                        break;
                    case "wisdom":
                        player.Wisdom++;
                        player.AttributePoints--;
                        await player.Save();
                        output = "You spend time consulting the wisdom of others, increasing your wisdom to **" + player.Wisdom + "**!";
                        break;
                    case "charisma":
                        player.Charisma++;
                        player.AttributePoints--;
                        await player.Save();
                        output = "You practice your charm, increasing your charisma to **" + player.Charisma + "**!";
                        break;
                    case "constitution":
                        player.Constitution++;
                        player.AttributePoints--;
                        await player.Save();
                        output = "You spend time improving your health, increasing your constitution to **" + player.Constitution + "**!";
                        break;
                    default:
                        output = "That's not a possible attribute. Please selected from the following:";
                        output += "\n[strength] [dexterity] [intelligence] [wisdom] [charisma] [constitution]";
                        break;
                }
            }
            else
            {
                output = "You need attribute points in order to train. You can gain these by leveling up or from rare items!";
            }
            await channel.SendMessageAsync(output);
        }

        private async Task Talk(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);
            InstancedNPC npc = await InstancedNPC.GetNpcInRoomByName(room, target);

            if (npc != null)
            {
                await OpportunityAttacks(player, room, channel);
                if (player != null && player.Alive)
                {
                    if (npc.EnemyID == player.ID)
                    {
                        await channel.SendMessageAsync("The " + npc.Name + " has no words for you, " + player.Name + ".");
                    }
                    else
                    {
                        string output = "";

                        Faction npcFaction = await npc.GetFaction();
                        FactionRep factionRep = null;
                        if (npcFaction != null)
                            factionRep = await FactionRep.GetFactionRepForPlayer(npcFaction, player.ID);

                        // Do we have a quest with this NPC?
                        QuestStep questStep = await QuestStep.GetCurrentQuestStepForPlayer(npc.SourceID, player.ID);
                        if (questStep != null)
                        {
                            // Check if this quest has a reputation requirement
                            int req = questStep.RepRequirement;
                            if (npcFaction != null)
                            {
                                if (factionRep.Reputation >= req * player.CharismaDiscount)
                                    output += questStep.StartPhrase;
                                else
                                    output += questStep.RepReqRejectionPhrase;
                            }
                            else
                            {
                                output += questStep.StartPhrase;
                            }
                        }

                        // We're a merchant, and need to explain our wares
                        if (npc.WaresCollectionID != null)
                        {
                            if ((npcFaction != null && factionRep.Reputation >= npc.WaresRepRequirement) || npcFaction == null)
                            {
                                output += npc.IntroPhrase;
                                output += "\n";
                                output += await npc.GetWaresListing(player);
                            }
                            else
                            {
                                output += npc.WaresRejectionPhrase;
                            }
                        }
                        else
                        {
                            // We're just a normal person, with a simple phrase to speak
                            output += npc.IntroPhrase;
                        }




                        await channel.SendMessageAsync(output);
                    }
                }
            }
            else
            {
                await channel.SendMessageAsync("You call out, but no one named " + target + " answers.");
            }
        }

        private async Task Use(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            string target1 = target;
            string target2 = "";

            if (target.Contains(" on "))
            {
                target1 = target.Substring(0, target.IndexOf(" on "));
                target2 = target.Substring(target1.Length + 4, target.Length - target1.Length - 4);
            }

            InstancedRoom room = await InstancedRoom.Get(player.RoomID);
            InstancedDoor door = await InstancedDoor.GetDoorInRoomByName(room, target1);
            InstancedItem item = await InstancedItem.GetPlayerItemByName(target1, player);

            if (door != null)
            {
                await OpportunityAttacks(player, room, channel);

                if (player.Alive)
                {
                    bool canAccess = true;
                    if (door.RequiredKey != null)
                    {
                        InstancedItem key = await player.GetItemBySourceID(door.RequiredKey);
                        if (key == null)
                            canAccess = false;
                    }
                    if (canAccess)
                    {
                        bool success = await FleeCombat(player, room, channel);
                        if (success)
                        {
                            string output = "";

                            // Check if this room has any traps
                            if (!String.IsNullOrEmpty(door.TrapID))
                            {
                                InstancedTrap trap = await InstancedTrap.Get(door.TrapID);
                                int savingThrowRoll = player.GetSavingThrow(trap.SavingThrow);
                                if(savingThrowRoll >= trap.SavingThrowRequirement)
                                {
                                    output += trap.OnSuccessMessage;
                                    player.RoomID = door.RoomDestination;
                                }
                                else
                                {
                                    Random rand = new Random();
                                    int damage = rand.Next(trap.MinDamage, trap.MaxDamage);
                                    await player.TakeDamage(damage);

                                    string failureMessage = trap.OnFailureMessage;
                                    failureMessage = failureMessage.Replace("{}", damage.ToString());
                                    output += failureMessage;
                                    
                                    if(player.Alive)
                                    {
                                        if(trap.AdvanceOnFailure)
                                        {
                                            player.RoomID = door.RoomDestination;
                                        }
                                        else
                                        {
                                            if(!String.IsNullOrEmpty(trap.FailureDestinationID))
                                                player.RoomID = trap.FailureDestinationID;
                                        }
                                        await player.Save();
                                    }
                                    else
                                    {
                                        await InstancedItem.DeleteAllPlayerItems(player.ID);
                                        output += "\n\nThe last of your life leaves your body, and you fall to your knees.  Your body is consumed by the greatness of Oculto.\nThank you for playing " + player.Name + ", I hope to see you again soon.";
                                        await player.Die();
                                    }
                                }
                            }
                            else
                            {
                                player.RoomID = door.RoomDestination;
                            }

                            if (player.Alive)
                            {
                                output += "\n";
                                output += door.OnUseOutput;
                            }

                            await player.Save();
                            await channel.SendMessageAsync(output);
                        }
                    }
                    else
                    {
                        await channel.SendMessageAsync("You are unable to use the " + door.Name + ".");
                    }
                }
            }
            else if (item != null)
            {
                if (!item.Unidentified)
                {
                    bool canUse = await item.CanUse();
                    if (canUse)
                    {
                        await OpportunityAttacks(player, room, channel);
                        if (player.Alive)
                        {
                            string result = "";

                            if (target2 != "")
                            {
                                // Determine if our target is an item in our possession or on the ground
                                InstancedItem targetItem = await InstancedItem.GetPlayerItemByName(target2, player);
                                if (targetItem == null)
                                    targetItem = await InstancedItem.GetPlayerItemByName(target2, player, true);
                                if (targetItem == null)
                                    targetItem = await InstancedItem.GetItemInRoomByName(room, target2);

                                if (targetItem != null)
                                {
                                    // We're using this item on another item
                                    result = await ItemEffects.Use(player, item, targetItem);
                                }
                                else
                                {
                                    // Determine if we're using this item on an NPC
                                    IWorldObject targetWorldObject = null;
                                    targetWorldObject = await InstancedNPC.GetNpcInRoomByName(room, target2);
                                    if (targetWorldObject == null)
                                        targetWorldObject = await InstancedPlayer.GetPlayerInRoomByName(room, target2);

                                    // We'll either have an NPC or a Player target, or pass a null so we try and cast without a target
                                    result = await ItemEffects.Use(player, item, targetWorldObject);
                                }
                            }
                            else
                            {
                                IWorldObject nothing = null;
                                result = await ItemEffects.Use(player, item, nothing);
                            }

                            await channel.SendMessageAsync(result);
                        }
                    }
                    else
                    {
                        await channel.SendMessageAsync("You lack the requirements to use this item.");
                    }
                }
                else
                {
                    await channel.SendMessageAsync("You can not use unidentified items!");
                }
            }
            else
            {
                await channel.SendMessageAsync("I don't understand what you're referring to.");
            }
        }

        private async Task Spellbook(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            string spells = await player.GetSpells();
            await channel.SendMessageAsync(spells);
        }

        private async Task Cast(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);

            string spellName = target;
            string targetName = "";

            if (target.Contains(" on "))
            {
                spellName = target.Substring(0, target.IndexOf(" on ")).ToLower();
                targetName = target.Substring(spellName.Length + 4, target.Length - spellName.Length - 4);

                Spell spell = await Utils.Spellbook.GetPlayerSpellByName(spellName, player.ID);

                if (spell != null)
                {
                    InstancedNPC npc = await InstancedNPC.GetNpcInRoomByName(room, targetName);
                    InstancedPlayer targetPlayer = await InstancedPlayer.GetPlayerInRoomByName(room, targetName);
                    InstancedItem targetItem = await InstancedItem.GetPlayerItemByName(targetName, player);
                    if (targetItem == null)
                        targetItem = await InstancedItem.GetPlayerItemByName(targetName, player, true);
                    if (targetItem == null)
                        targetItem = await InstancedItem.GetItemInRoomByName(room, targetName);

                    if (npc != null)
                    {
                        await OpportunityAttacks(player, room, channel);
                        if (player.Alive)
                            await channel.SendMessageAsync(await SpellEffects.Use(player, npc, spell));
                    }
                    else if (targetPlayer != null)
                    {
                        await OpportunityAttacks(player, room, channel);
                        if (player.Alive)
                            await channel.SendMessageAsync(await SpellEffects.Use(player, targetPlayer, spell));
                    }
                    else if (targetItem != null)
                    {
                        await OpportunityAttacks(player, room, channel);
                        if (player.Alive)
                            await channel.SendMessageAsync(await SpellEffects.Use(player, targetItem, spell));
                    }
                    else
                    {
                        await channel.SendMessageAsync("I don't understand who or what you want to cast this on.");
                    }
                }
                else
                {
                    await channel.SendMessageAsync("You don't know that spell.");
                }
            }
            else
            {
                await channel.SendMessageAsync("Please specify who you want to cast this spell on!");
            }
        }

        private async Task Drop(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedItem item = await InstancedItem.GetPlayerItemByName(target, player);

            if (item != null)
            {
                item.RoomID = player.RoomID;
                item.PlayerID = null;
                await item.Save();
                await channel.SendMessageAsync("You drop your " + target + ".");
            }
            else
            {
                //Try dropping equipment
                item = await InstancedItem.GetPlayerItemByName(target, player, true);
                if (item != null)
                {
                    await player.UnequipItem(item);
                    item.RoomID = player.RoomID;
                    item.PlayerID = null;
                    await item.Save();
                    await channel.SendMessageAsync("You unequip and drop your " + target + ".");
                }
                else
                {
                    await channel.SendMessageAsync("You don't have a " + target + " to drop.");
                }
            }
        }

        private async Task Buy(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);

            //Check for opportunity attacks
            await OpportunityAttacks(player, room, channel);

            if (player.Alive)
            {
                //Check to see if any NPCs have the requested items in their wares
                List<InstancedNPC> npcs = await InstancedNPC.GetNPCsForRoom(room);
                Item item = null;
                if (npcs.Count > 0)
                {
                    foreach (InstancedNPC npc in npcs)
                    {
                        if (npc.EnemyID != player.ID)
                        {
                            item = await npc.GetWaresByName(target);
                            if (item != null)
                                break;
                        }
                    }
                }

                if (item == null)
                {
                    await channel.SendMessageAsync("You aren't able to purchase a " + target + ".");
                }
                else
                {
                    int adjustedPrice = (int)(item.Price * player.CharismaDiscount);
                    bool purchased = await player.SpendGold(adjustedPrice);
                    if (purchased)
                    {
                        InstancedItem instancedItem = new InstancedItem(item);
                        instancedItem.PlayerID = player.ID;
                        await instancedItem.Save();

                        await channel.SendMessageAsync("You receive a **" + item.Name + "**.");
                    }
                    else
                    {
                        await channel.SendMessageAsync("You don't have the gold to purchase a " + item.Name + ".");
                    }
                }
            }
        }

        private async Task Sell(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);

            //Check if we actually have what we're trying to sell
            InstancedItem item = await InstancedItem.GetPlayerItemByName(target, player);

            if (item != null)
            {
                await OpportunityAttacks(player, room, channel);

                if (player.Alive)
                {
                    //Check to see if any NPCs have wares - making them a merchant
                    List<InstancedNPC> npcs = await InstancedNPC.GetNPCsForRoom(room);
                    bool merchantFound = false;

                    if (npcs.Count > 0)
                    {
                        foreach (InstancedNPC npc in npcs)
                        {
                            if (npc.EnemyID != player.ID)
                            {
                                if (npc.WaresCollectionID != null)
                                    merchantFound = true;
                            }
                        }
                    }

                    if (merchantFound)
                    {
                        int itemValue = item.Price / 2;
                        await player.DestroyItem(item.ID);
                        await player.GiveGold(itemValue);

                        await channel.SendMessageAsync("You sell your " + target + " for " + itemValue + " gold.");
                    }
                    else
                    {
                        await channel.SendMessageAsync("There is no one around to buy your " + target + ".");
                    }
                }
            }
            else
            {
                await channel.SendMessageAsync("You dont have a " + target + " in your inventory.");
            }
        }

        private async Task Inventory(InstancedPlayer player, ISocketMessageChannel channel)
        {
            string inventory = await player.GetInventoryListing();
            await channel.SendMessageAsync(inventory);
        }

        private async Task Attack(InstancedPlayer player, ISocketMessageChannel channel, string target)
        {
            InstancedRoom room = await InstancedRoom.Get(player.RoomID);
            InstancedNPC npc = await InstancedNPC.GetNpcInRoomByName(room, target);
            string output = "";
            if (npc != null)
            {
                int roll = await player.AttackRoll();
                int damage = await player.GetDamage();
                string weaponName = await player.GetWeaponName();
                string thePrefix = Char.IsUpper(npc.Name[0]) ? "" : "the ";
                string startingThePrefix = Char.IsUpper(npc.Name[0]) ? "" : "The ";

                //Set us as the enemy of the NPC so it continues to attack us or block us from leaving
                await npc.SetEnemy(player);

                StatBlock npcStatBlock = await npc.GetStatblock();
                if (roll >= npcStatBlock.AC)
                {
                    Random rand = new Random();
                    bool crit = rand.Next(1, 21) == 20;

                    if (crit)
                    {
                        await npc.TakeDamage(damage * 2);
                        output += "You attack with your " + weaponName + ". You critically hit, dealing " + (damage * 2) + " damage to " + thePrefix + npc.Name + "!";
                    }
                    else
                    {
                        await npc.TakeDamage(damage);
                        output += "You attack with your " + weaponName + ", dealing " + damage + " damage to " + thePrefix + npc.Name + ".";
                    }
                }
                else
                {
                    if (npcStatBlock.AC - roll < 3)
                        output += "You attack with your " + weaponName + ", but the attack barely grazes " + thePrefix + npc.Name + "!";
                    else if (npcStatBlock.AC - roll < 6)
                        output += "You attack with your " + weaponName + ", but the attack misses " + thePrefix + npc.Name + ".";
                    else
                        output += "You attack with your " + weaponName + ", but don't even get close to dealing damage.";
                }

                if (!npc.Alive)
                {
                    await npc.SetEnemy(null);

                    output += "\nYou kill " + thePrefix + npc.Name + ".";
                    if (npc.LootTableID != null)
                    {
                        LootTable lootTable = await LootTable.Get(npc.LootTableID);
                        List<Item> drops = await lootTable.GetLootDrops();
                        foreach (Item item in drops)
                        {
                            output += "\n" + npc.Name + " drops a **" + item.Name + "**.";
                            InstancedItem instancedItem = new InstancedItem(item);
                            instancedItem.RoomID = room.ID;
                            await instancedItem.Save();
                        }
                    }

                    if (npcStatBlock.FactionID != null && npcStatBlock.FactionDeathPenalty != 0)
                    {
                        Faction faction = await Faction.Get(npcStatBlock.FactionID);
                        if (faction != null)
                        {
                            FactionRep factionRep = await FactionRep.GetFactionRepForPlayer(faction, player.ID);
                            factionRep.Reputation -= npcStatBlock.FactionDeathPenalty;
                            if (factionRep.Reputation < 0)
                                factionRep.Reputation = 0;
                            await factionRep.Save();

                            output += "\nYou lose " + npcStatBlock.FactionDeathPenalty + " reputation with " + faction.Name + ".";
                        }
                    }

                    bool levelUp = await player.AddXP(npcStatBlock.DeathXP);
                    output += "\n\nKilling " + thePrefix + npc.Name + " awards you " + npcStatBlock.DeathXP + " XP.";
                    if (levelUp)
                        output += "\nYou level up! You are now level **" + player.CurrentLevel + "**!";
                }
                else
                {
                    roll = await npc.Roll();
                    damage = await npc.GetDamage();
                    int ac = await player.GetAC();

                    if (roll >= ac)
                    {
                        Random rand = new Random();
                        bool crit = rand.Next(1, 21) == 20;

                        if (crit)
                        {
                            await player.TakeDamage(damage * 2);
                            output += "\n" + startingThePrefix + npc.Name + " attacks back, they deal a critical hit, dealing " + (damage * 2) + " damage!";
                        }
                        else
                        {
                            await player.TakeDamage(damage);
                            output += "\n" + startingThePrefix + npc.Name + " attacks back, dealing " + damage + " damage!";
                        }
                    }
                    else
                    {
                        if (ac - roll < 3)
                            output += "\n" + startingThePrefix + npc.Name + " attacks back, but the attack barely grazes you!";
                        else if (ac - roll < 6)
                            output += "\n" + startingThePrefix + npc.Name + " attacks back, but the attack misses you.";
                        else
                            output += "\n" + startingThePrefix + npc.Name + " attacks back, but doesn't even get close to dealing damage.";
                    }

                    if (!player.Alive)
                    {
                        await InstancedItem.DeleteAllPlayerItems(player.ID);
                        output += "\n\nThe last of your life leaves your body, and you fall to your knees.  Your body is consumed by the greatness of Oculto.\nThank you for playing " + player.Name + ", I hope to see you again soon.";
                        await player.Die();
                    }
                }
            }
            else
            {
                //PvP
                InstancedPlayer targetPlayer = await InstancedPlayer.GetPlayerInRoomByName(room, target);
                if (targetPlayer != null)
                {
                    int damage = await player.GetDamage();
                    string weaponName = await player.GetWeaponName();

                    int playerAC = await player.GetAC();
                    int targetAC = await targetPlayer.GetAC();

                    int roll = await player.AttackRoll();

                    if (targetPlayer.Resting)
                    {
                        output += "As you make your way towards " + targetPlayer.Name + ", they are awoken from their rest!\n";
                        targetPlayer.Resting = false;
                        await targetPlayer.Save();
                    }

                    if (roll >= targetAC)
                    {
                        Random rand = new Random();
                        bool crit = rand.Next(1, 21) == 20;

                        if (crit)
                        {
                            await targetPlayer.TakeDamage(damage * 2);
                            output += "You attack with your " + weaponName + " and deal a critical hit! You deal " + (damage * 2) + " damage to " + targetPlayer.Name + "!";
                        }
                        else
                        {
                            await targetPlayer.TakeDamage(damage);
                            output += "You attack with your " + weaponName + ", dealing " + damage + " damage to " + targetPlayer.Name + "!";
                        }
                    }
                    else
                    {
                        if (targetAC - roll < 3)
                            output += "You attack with your " + weaponName + ", but the attack barely grazes " + targetPlayer.Name + "!";
                        else if (targetAC - roll < 6)
                            output += "You attack with your " + weaponName + ", but the attack misses " + targetPlayer.Name + ".";
                        else
                            output += "You attack with your " + weaponName + ", but don't even get close to dealing damage.";
                    }

                    if (!targetPlayer.Alive)
                    {
                        output += "\nYou kill " + target + ".  Their body falls limp to the ground, a worthy sacrifice to the lord of bones.";
                        output += await targetPlayer.DropAllItems(room);

                        await targetPlayer.Die();
                    }
                    else if (targetPlayer.ID != player.ID)
                    {
                        damage = await targetPlayer.GetDamage();
                        weaponName = await targetPlayer.GetWeaponName();

                        roll = await targetPlayer.AttackRoll();

                        if (roll >= playerAC)
                        {
                            Random rand = new Random();
                            bool crit = rand.Next(1, 21) == 20;

                            if (crit)
                            {
                                await player.TakeDamage(damage * 2);
                                output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ".  They deal a critical strike dealing " + damage + " damage!";
                            }
                            else
                            {
                                await player.TakeDamage(damage);
                                output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ", dealing " + damage + " damage!";
                            }
                        }
                        else
                        {
                            if (playerAC - roll < 3)
                                output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ", but the attack barely grazes you!";
                            else if (playerAC - roll < 6)
                                output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ", but the attack misses you.";
                            else
                                output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ", but don't even get close to dealing damage.";
                        }

                        if (!player.Alive)
                        {
                            //Drop inventory if you died from PvP
                            output += await player.DropAllItems(room);
                            output += "\n\nThe last of your life leaves your body, and you fall to your knees before collapsing.  Your body is consumed by the greatness of Oculto.\nThank you for playing " + player.Name + ", I hope to see you again soon.";

                            await player.Die();
                        }
                    }
                }
            }
            if (output == "")
                output = "You don't see a " + target + ".";

            await channel.SendMessageAsync(output);
        }

        private async Task Rep(InstancedPlayer player, ISocketMessageChannel channel)
        {
            string output = "===== Reputation =====";
            List<Faction> factions = await Faction.GetAll();
            foreach (Faction faction in factions)
            {
                FactionRep factionRep = await FactionRep.GetFactionRepForPlayer(faction, player.ID);
                output += "\n" + faction.Name + ": " + factionRep.Reputation;
            }
            await channel.SendMessageAsync(output);
        }

        private async Task Suicide(InstancedPlayer player, ISocketMessageChannel channel)
        {
            string output = "";
            await player.TakeDamage(player.HitPoints);
            if (!player.Alive)
            {
                await InstancedItem.DeleteAllPlayerItems(player.ID);
                output += "\n\nYou curse the god of this world, and your body is consumed by black mist. Your body is consumed by the greatness of Oculto.\nThank you for playing " + player.Name + ", I hope to see you again soon.";
                await player.Die();
            }

            await channel.SendMessageAsync(output);
        }

        private async Task<bool> FleeCombat(InstancedPlayer player, InstancedRoom room, ISocketMessageChannel channel)
        {
            string output = "";
            List<InstancedNPC> npcs = await InstancedNPC.GetNPCsForRoom(room);
            Random rand = new Random();
            bool escaped = true;
            foreach (InstancedNPC npc in npcs)
            {
                //Enemies have a 50% chance of preventing escape
                if (npc.EnemyID != null && npc.EnemyID != "")
                {
                    if (npc.EnemyID == player.ID)
                    {
                        int escape = rand.Next(0, 2);
                        if (escape == 0)
                        {
                            escaped = false;
                            output = "The " + npc.Name + " blocks your path, preventing your escape!";
                        }
                    }
                }
            }
            if (output != "")
                await channel.SendMessageAsync(output);
            return escaped;
        }

        private async Task OpportunityAttacks(InstancedPlayer player, InstancedRoom room, ISocketMessageChannel channel)
        {
            string output = "";
            List<InstancedNPC> npcs = await InstancedNPC.GetNPCsForRoom(room);
            foreach (InstancedNPC npc in npcs)
            {
                if (npc.EnemyID != null && npc.EnemyID != "")
                {
                    if (npc.EnemyID == player.ID)
                    {
                        int roll = await npc.Roll();
                        int damage = await npc.GetDamage();
                        int ac = await player.GetAC();

                        if (roll >= ac)
                        {
                            if (roll >= 20)
                            {
                                await player.TakeDamage(damage * 2);
                                output += "\nThe " + npc.Name + " finds an opportunity to attack, and they strike critically, dealing " + (damage * 2) + " damage!";
                            }
                            else
                            {
                                await player.TakeDamage(damage);
                                output += "\nThe " + npc.Name + " finds an opportunity to attack, dealing " + damage + " damage!";
                            }
                        }
                        else
                        {
                            if (ac - roll < 3)
                                output += "\nThe " + npc.Name + " attacks, but the attack barely grazes you!";
                            else if (ac - roll < 6)
                                output += "\nThe " + npc.Name + " attacks, but the attack misses you.";
                            else
                                output += "\nThe " + npc.Name + " attacks, but doesn't even get close to dealing damage.";
                        }

                        if (!player.Alive)
                        {
                            await InstancedItem.DeleteAllPlayerItems(player.ID);
                            output += "\n\nThe last of your life leaves your body, and you fall to your knees.  Your body is consumed by the greatness of Oculto.\nThank you for playing " + player.Name + ", I hope to see you again soon.";
                            await player.Die();
                        }
                    }
                }
            }
            if (output != "")
                await channel.SendMessageAsync(output);
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
