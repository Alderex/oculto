﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shared.Models.Definitions;
using Shared;
using OcultoBot.Models;

namespace OcultoBot.Utils
{
    public class Spellbook
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string PlayerID { get; set; }
        public string SpellID { get; set; }

        public Spellbook() { }

        public static async Task<Spellbook> Get(string ID)
        {
            return await Database.Instance.Connection.Table<Spellbook>().Where(i => i.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(Spellbook spellbook)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(spellbook);
        }

        public static async Task<int> Delete(Spellbook spellbook)
        {
            return await Database.Instance.Connection.DeleteAsync(spellbook);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<Spellbook>();
        }

        public static async Task AddSpell(string playerID, string spellName)
        {
            Spell spell = await Spell.GetByName(spellName);
            if(spell != null)
            {
                Spellbook spellbook = new Spellbook();
                spellbook.ID = Guid.NewGuid().ToString();
                spellbook.PlayerID = playerID;
                spellbook.SpellID = spell.ID;
                await Database.Instance.Connection.InsertOrReplaceAsync(spellbook);
            }
        }

        public static async Task<Spell> GetPlayerSpellByName(string spellName, string playerID)
        {
            List<Spell> playerSpells = await GetSpells(playerID);
            Spell spell = playerSpells.Where(s => s.Name == spellName).FirstOrDefault();
            return spell;
        }

        public static async Task<List<Spell>> GetSpells(string playerID)
        {
            List<Spellbook> spellbook = await Database.Instance.Connection.Table<Spellbook>().Where(x => x.PlayerID == playerID).ToListAsync();
            List<Spell> spells = new List<Spell>();
            foreach (Spellbook page in spellbook)
            {
                Spell spell = await Spell.Get(page.SpellID);
                if(spell != null)
                    spells.Add(spell);
            }

            // This doesn't really go here, but it's convenient - Find all equipment that gives us a spell as well
            List<InstancedItem> imbuedEquipment = await InstancedItem.GetPlayerImbuedEquipment(playerID);
            foreach (InstancedItem item in imbuedEquipment)
            {
                Spell spell = await Spell.Get(item.ImbuedSpellID);
                spells.Add(spell);
            }

            return spells;
        }
    }
}
