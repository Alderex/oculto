﻿using SQLite;
using Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Shared.Models.Utils;

namespace OcultoBot.Utils
{
    public class FactionRep
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string FactionID { get; set; }
        public string PlayerID { get; set; }
        public int Reputation { get; set; }

        public static async Task<FactionRep> Get(string ID)
        {
            return await Database.Instance.Connection.Table<FactionRep>().Where(x => x.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<List<FactionRep>> GetAll()
        {
            return await Database.Instance.Connection.Table<FactionRep>().OrderBy(x => x.FactionID).ToListAsync();
        }

        public static async Task<List<FactionRep>> GetAllForPlayer(string playerID)
        {
            return await Database.Instance.Connection.Table<FactionRep>().Where(x => x.PlayerID == playerID).ToListAsync();
        }

        public static async Task<FactionRep> GetFactionRepForPlayer(Faction faction, string playerID)
        {
            FactionRep rep = await Database.Instance.Connection.Table<FactionRep>().Where(x => x.FactionID == faction.ID && x.PlayerID == playerID).FirstOrDefaultAsync();
            if(rep == null)
            {
                rep = new FactionRep
                {
                    ID = Guid.NewGuid().ToString(),
                    FactionID = faction.ID,
                    PlayerID = playerID,
                    Reputation = 100
                };
                await rep.Save();
            }

            return rep;
        }

        public static async Task<int> Assert(FactionRep factionRep)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(factionRep);
        }

        public static async Task<int> Delete(FactionRep factionRep)
        {
            return await Database.Instance.Connection.DeleteAsync(factionRep);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<FactionRep>();
        }

        public async Task Save()
        {
            await Assert(this);
        }
    }
}
