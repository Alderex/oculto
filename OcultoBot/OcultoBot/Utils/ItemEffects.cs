﻿using OcultoBot.Models;
using Shared;
using Shared.Models.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OcultoBot.Utils
{
    public class ItemEffects
    {
        // Effects that can target either a WorldObject or just the player who used it
        public static async Task<string> Use(InstancedPlayer player, InstancedItem item, IWorldObject target)
        {
            if(item.OnUseEffect == "Scroll" && target == null)
                return "Please specify who you want to cast this spell on!";

            if (item.ConsumeOnUse)
            {
                await player.DestroyItem(item.ID);
            }

            int amount = 0;
            string output = "";

            switch (item.OnUseEffect)
            {
                case "None":
                    if (!String.IsNullOrEmpty(item.OnUseOutput))
                        return item.OnUseOutput;
                    return "Nothing happens.";
                case "Heal":
                    Int32.TryParse(item.OnUseParameter, out amount);
                    int healAmount = await player.Heal(amount);
                    output = item.OnUseOutput;
                    output += "\nYou heal for " + healAmount + " HP!";
                    return output;
                case "RestoreMana":
                    Int32.TryParse(item.OnUseParameter, out amount);
                    int manaRestored = await player.RestoreMana(amount);
                    output = item.OnUseOutput;
                    output += "\nYou restore " + manaRestored + " mana!";
                    return output;
                case "LearnSpell":
                    await player.LearnSpell(item.OnUseParameter);
                    return item.OnUseOutput;
                case "Rest":
                    Int32.TryParse(item.OnUseParameter, out amount);
                    await player.Rest(amount);
                    return item.OnUseOutput;
                case "Scroll":
                    Spell spell = await Spell.GetByName(item.OnUseParameter);
                    if(spell != null)
                        return await SpellEffects.Use(player, target, spell, false);
                    return "There was a problem using this scroll.";
                case "Teleport":
                    player.RoomID = item.OnUseParameter;
                    await InstancedPlayer.Assert(player);
                    return item.OnUseOutput;
                default:
                    return "Nothing happens.";
            }
        }

        // Effects that target an item
        public static async Task<string> Use(InstancedPlayer player, InstancedItem item, InstancedItem target)
        {
            if (item.ConsumeOnUse)
            {
                await player.DestroyItem(item.ID);
            }

            switch (item.OnUseEffect)
            {
                case "None":
                    if (!String.IsNullOrEmpty(item.OnUseOutput))
                        return item.OnUseOutput;
                    return "Nothing happens.";
                case "Scroll":
                    Spell spell = await Spell.GetByName(item.OnUseParameter);
                    if (spell != null)
                        return await SpellEffects.Use(player, target, spell, false);
                    return "There was a problem using this scroll.";
                default:
                    return "Nothing happens.";
            }
        }
    }
}
