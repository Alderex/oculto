﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OcultoBot.Models;
using Shared.Models.Definitions;
using Shared.Models.Utils;
using Shared.Utils;

namespace OcultoBot.Utils
{
    public class SpellEffects
    {
        public static async Task<string> Use(InstancedPlayer caster, InstancedItem target, Spell spell, bool useMana = true)
        {
            string output = "You lack sufficient mana for this spell.";

            if (caster.Mana >= spell.ManaCost || !useMana)
            {
                InstancedRoom room = await InstancedRoom.Get(caster.RoomID);

                if (useMana)
                    caster.Mana -= spell.ManaCost;

                if (spell.SpellType == SpellType.IDENTIFY)
                {
                    if(target.Unidentified)
                    {
                        // Find the source item
                        Item sourceItem = await Item.Get(target.SourceID);

                        if(sourceItem != null)
                        {
                            target.Unidentified = false;
                            target.Name = sourceItem.Name;
                            target.Description = sourceItem.Description;
                            await target.Save();
                            return "You identified the item as a " + target.Name + "!";
                        }
                        else
                        {
                            return "There was a problem identifying the " + target.Name + "!";
                        }
                    }
                }
            }

            await caster.Save();

            return output;
        }

        public static async Task<string> Use(IWorldObject caster, IWorldObject target, Spell spell, bool useMana = true)
        {
            if (caster.Mana >= spell.ManaCost || !useMana)
            {
                InstancedPlayer player = (InstancedPlayer)caster;
                InstancedRoom room = await InstancedRoom.Get(player.RoomID);

                if(useMana)
                    player.Mana -= spell.ManaCost;

                string output = "";

                if (spell.SpellType == SpellType.DAMAGE)
                {
                    if (target.GetType() == typeof(InstancedNPC))
                    {
                        InstancedNPC npc = (InstancedNPC)target;
                        StatBlock npcStatBlock = await npc.GetStatblock();

                        output = spell.OnUseOutput.Replace("{}", target.Name);
                        output += "\nYou deal " + spell.Amount + " damage to the " + target.Name + "!";
                        await target.TakeDamage(spell.Amount);

                        if (!npc.Alive)
                        {
                            await npc.SetEnemy(null);
                            output += "\nYou kill the " + npc.Name + ".";
                            LootTable lootTable = await LootTable.Get(npc.LootTableID);
                            List<Item> droppedItems = await lootTable.GetLootDrops();
                            // Create instances of the items that the NPC generated on death
                            foreach (Item item in droppedItems)
                            {
                                InstancedItem instancedItem = new InstancedItem(item);
                                instancedItem.RoomID = player.RoomID;
                                await InstancedItem.Assert(instancedItem);
                                output += "\n" + npc.Name + " drops a **" + item.Name + "**.";
                            }

                            bool levelUp = await player.AddXP(npcStatBlock.DeathXP);
                            output += "\n\nKilling the " + npc.Name + " awards you " + npcStatBlock.DeathXP + " XP.";
                            if (levelUp)
                                output += "\nYou level up! You are now level " + player.CurrentLevel + "!";

                            //await db.Delete(npc);
                        }
                        else
                        {
                            int roll = await npc.Roll();
                            int damage = await npc.GetDamage();
                            int ac = await player.GetAC();

                            if (roll >= ac)
                            {
                                Random rand = new Random();
                                bool crit = rand.Next(1, 21) == 20;

                                if (crit)
                                {
                                    await player.TakeDamage(damage * 2);
                                    output += "\nThe " + target.Name + " attacks back, they deal a critical hit, dealing " + (damage * 2) + " damage!";
                                }
                                else
                                {
                                    await player.TakeDamage(damage);
                                    output += "\nThe " + target.Name + " attacks back, dealing " + damage + " damage!";
                                }
                            }
                            else
                            {
                                if (ac - roll < 3)
                                    output += "\nThe " + target.Name + " attacks back, but the attack barely grazes you!";
                                else if (ac - roll < 6)
                                    output += "\nThe " + target.Name + " attacks back, but the attack misses you.";
                                else
                                    output += "\nThe " + target.Name + " attacks back, but doesn't even get close to dealing damage.";
                            }

                            if (!player.Alive)
                            {
                                await InstancedItem.DeleteAllPlayerItems(player.ID);
                                output += "\n\nThe last of your life leaves your body, and you fall to your knees.  Your body is consumed by the greatness of Oculto.\nThank you for playing " + player.Name + ", I hope to see you again soon.";
                                await player.Die();
                            }
                        }
                    }
                    else if (target.GetType() == typeof(InstancedPlayer))
                    {
                        InstancedPlayer targetPlayer = (InstancedPlayer)target;

                        output = spell.OnUseOutput.Replace("{}", target.Name);
                        output += "\nYou deal " + spell.Amount + " damage to " + target.Name + "!";
                        await target.TakeDamage(spell.Amount);

                        if (!targetPlayer.Alive)
                        {
                            output += targetPlayer.DropAllItems(room);
                            output += "\nYou kill " + target.Name + ".  Their body falls limp to the ground, a worthy sacrifice to the lord of bones.";
                            await targetPlayer.Die();
                        }
                        else if (targetPlayer.ID != player.ID)
                        {
                            int damage = await targetPlayer.GetDamage();
                            string weaponName = await targetPlayer.GetWeaponName();

                            int roll = targetPlayer.Roll();
                            int playerAC = await player.GetAC();

                            if (roll >= playerAC)
                            {
                                Random rand = new Random();
                                bool crit = rand.Next(1, 21) == 20;

                                if (crit)
                                {
                                    await player.TakeDamage(damage * 2);
                                    output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ".  They deal a critical strike dealing " + damage + " damage!";
                                }
                                else
                                {
                                    await player.TakeDamage(damage);
                                    output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ", dealing " + damage + " damage!";
                                }
                            }
                            else
                            {
                                if (playerAC - roll < 3)
                                    output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ", but the attack barely grazes you!";
                                else if (playerAC - roll < 6)
                                    output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ", but the attack misses you.";
                                else
                                    output += "\n" + targetPlayer.Name + " attacks back with their " + weaponName + ", but don't even get close to dealing damage.";
                            }

                            if (!player.Alive)
                            {
                                output += player.DropAllItems(room);
                                output += "\n\nThe last of your life leaves your body, and you fall to your knees before collapsing. Your body is consumed by the greatness of Oculto.\nThank you for playing " + player.Name + ", I hope to see you again soon.";

                                await player.Die();
                            }
                        }
                    }
                }
                else if(spell.SpellType == SpellType.HEAL)
                {
                    int healAmount = await target.Heal(spell.Amount);
                    output += "You heal " + target.Name + " for " + healAmount + " HP!";
                }
                else
                {
                    output += "Your spell has no effect!";
                }

                if(caster != null)
                    await caster.Save();
                if(target != null)
                    await target.Save();

                return output;
            }
            else
            {
                return "You lack sufficient mana for this spell.";
            }
        }
    }
}
