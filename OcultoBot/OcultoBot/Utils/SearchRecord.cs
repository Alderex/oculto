﻿using Shared;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OcultoBot.Utils
{
    public class SearchRecord
    {
        [PrimaryKey]
        public string ID { get; set; }
        public string PlayerID { get; set; }
        public string RoomID { get; set; }
        public int Roll { get; set; }
        public DateTime Timestamp { get; set; }

        public SearchRecord() { }

        public static async Task<SearchRecord> Get(string ID)
        {
            return await Database.Instance.Connection.Table<SearchRecord>().Where(s => s.ID == ID).FirstOrDefaultAsync();
        }

        public static async Task<int> Assert(SearchRecord searchRecord)
        {
            return await Database.Instance.Connection.InsertOrReplaceAsync(searchRecord);
        }

        public static async Task<int> Delete(SearchRecord searchRecord)
        {
            return await Database.Instance.Connection.DeleteAsync(searchRecord);
        }

        public static async Task<int> DeleteAll()
        {
            return await Database.Instance.Connection.DeleteAllAsync<SearchRecord> ();
        }

        public static async Task DeletePlayerSearchRecords(string playerID)
        {
            await Database.Instance.Connection.Table<SearchRecord>().DeleteAsync(x => x.PlayerID == playerID);
        }

        public static async Task<SearchRecord> GetBestSearchRecordForRoom(string playerID, string roomID)
        {
            return await Database.Instance.Connection.Table<SearchRecord>().Where(x => x.PlayerID == playerID && x.RoomID == roomID).OrderByDescending(x => x.Roll).FirstOrDefaultAsync();
        }

        public static async Task<SearchRecord> GetLatestSearchRecordForRoom(string playerID, string roomID)
        {
            return await Database.Instance.Connection.Table<SearchRecord>().Where(x => x.PlayerID == playerID && x.RoomID == roomID).OrderByDescending(x => x.Timestamp).FirstOrDefaultAsync();
        }

        public static async Task SaveSearch(string playerID, string roomID, int roll)
        {
            SearchRecord searchRecord = new SearchRecord();
            searchRecord.ID = Guid.NewGuid().ToString();
            searchRecord.PlayerID = playerID;
            searchRecord.RoomID = roomID;
            searchRecord.Roll = roll;
            searchRecord.Timestamp = DateTime.Now;
            await Database.Instance.Connection.InsertAsync(searchRecord);
        }
    }
}
